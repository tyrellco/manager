/*
 *      matches.js
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */



function do_get_full_report(jornada){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmatches.php",
					data: "opt=1&jornada="+jornada,
					success:function(xml) {
										$("#loading").hide();
										var numjornada; //jornada de la antiliga.
										$(xml).find('match').each(function(){
											numjornada=parseInt($(this).find('jornada_antiliga').text());
										});
										var team="myteam";
										var localhtml=$(xml).find('local_tablehtml').html();
										var guesthtml=$(xml).find('enemy_tablehtml').html();
										//Jornada (antiliga) par jugamos en casa, impar jugamos fuera
										if (numjornada%2!=0){
											team="myenemy";
											localhtml=$(xml).find('enemy_tablehtml').html();
											guesthtml=$(xml).find('local_tablehtml').html();
										}
											
											
										var estadio="";
										$(xml).find(team).each(function(){
											estadio=$(this).find('estadio').text();
										});
	
										$("#leftcontainer").html($(xml).find('htmlgrass').html());
										$("#rightcontainer #localtable").html(localhtml);
										$("#rightcontainer #guesttable").html(guesthtml);
										$("#rightcontainer #infomatch").html(estadio);
										$("div[id^=_]").bind("click",function(){
											do_set_form_numbers($(this).attr("id"),$(this).children().text());
										});
									}
				});
}

function do_update_list_goalsandreds (jornada){
	$("#loading").show();
	
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmatches.php",
					data: "opt=4&jornada="+jornada,
					success:function(data) {
										$("#loading").hide();
										$("#containgr_local").html(data);
									}
	});
}

function do_set_numbers (goals,reds,idalineacion,idpos,idjugador,playername){
$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmatches.php",
					data: "opt=3&goals="+goals+"&reds="+reds+"&idalineacion="+idalineacion+"&idpos="+idpos+"&idjugador="+idjugador,
					success:function(data) {
										$("#loading").hide();
										$("#popup").fadeOut(800);
										var idjugador="#"+idpos;
										var goalhtml="";
										var idgoles="#"+idpos+" div.goles";
										$(idgoles).remove();
										if (goals > 0){
											goalhtml="<div class='goles'>";
											for (i=0; i<goals; i++){
												goalhtml +="<img src='images/gol.png'>";
											}	
											goalhtml +="</div>";
											var oldhtml=$(idjugador).html();
											$(idjugador).html(oldhtml+goalhtml);
											
											
										}

										var idred="#"+idpos+" p";
										$(idred).removeAttr("class");
										if (reds==1)
											$(idred).attr("class","roja");
										else
											$(idred).attr("class","placebo");
											
										do_update_list_goalsandreds($("#listformmatches").val());	
									}
				});
}

function do_set_form_numbers(idpos,playername){
$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmatches.php",
					data: "opt=2&idpos="+idpos,
					success:function(data) {
										$("#loading").hide();
										$("#popup").fadeIn(800);
										$("#popup").html(data);
										$("#form_edit_numbers #goals").bind("blur",function(){
											//Eliminamos los espacios en blanco al princio y final de la cadena
											var value=$(this).val().replace(/^\s*|\s*$/g,"");
											// isNaN devuelve false (0) si es un entero
											if ( isNaN(value) || (value<0) || (value.length==0) ){
												alert("Tienes que introducir un número");	
												$(this).val(0);
											}
											else{
												if (value > 10){
													alert("Tantos goles ??,no creo");	
													$(this).val(0);
												} 
											}		
										});
										$("#form_edit_numbers #btn_cancel_form_numbers").bind("click",function(){
											$("#popup").fadeOut(800);
										});
										$("#form_edit_numbers #btn_set_form_numbers").bind("click",function(){
											do_set_numbers($("#form_edit_numbers #goals").val(),$("#form_edit_numbers #reds").val(),
											$("#form_edit_numbers #idalineacion").val(),idpos,$("#form_edit_numbers #idjug").val(),playername);
											$("#popup").fadeOut(800);
										});
									}
				});
}
