/*
 *      login.js
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function do_login(user,passwd){
	$("#msglogin").removeAttr("class");
	$("#msglogin").addClass("info");
	$("#msglogin").text("conectando...");
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxlogin.php",
					data: "opt=1&user="+user+"&passwd="+passwd,
					success:function(data) {
										if (data==0){
											setTimeout(function(){
												$("#msglogin").addClass("info");
												$("#msglogin").text("adelante...");
												},1000);
											setTimeout("window.location.href='desktop.php'",2000);
										}	
										else{
											setTimeout(function(){
												$("#msglogin").addClass("error");
												$("#msglogin").text("usuario o password, no entras");
												},1000);
											
											
										}	
									}
		});
}
