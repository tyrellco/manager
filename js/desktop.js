/*
 *      desktop.js
 *
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function load_menu_links(roluser, status) {
  if (status == 0) {
    $("#main_menu ul li#menu_1").bind("click", function () {
      window.location.href = "m.manager.php";
    });
    $("#main_menu ul li#menu_2").bind("click", function () {
      window.location.href = "matches.php";
    });
    $("#main_menu ul li#menu_3").bind("click", function () {
      window.location.href = "myteam.php";
    });
  } else {
    var msg = "Opción deshabilitada, primero debes configurar tu equipo";
    $("#main_menu ul li#menu_1").bind("click", function () {
      alert(msg);
    });
    $("#main_menu ul li#menu_2").bind("click", function () {
      alert(msg);
    });
    $("#main_menu ul li#menu_3").bind("click", function () {
      alert(msg);
    });
  }

  $("#main_menu ul li#menu_4").bind("click", function () {
    window.location.href = "configure.php";
  });
  $("#main_menu ul li#menu_5").bind("click", function () {
    if (roluser == 1) window.location.href = "admin.php";
    else alert("No tienes permiso para acceder a esta zona, no pasas");
  });
}

function init_button_results() {
  $("td.next").bind("click", function () {
    do_get_lastest_results($("input#next_id").val());
  });
  $("td.prev").bind("click", function () {
    do_get_lastest_results($("input#prev_id").val());
  });
}

function do_get_lastest_results(idjornada) {
  $("#loading").show();
  $.ajax({
    type: "POST",
    url: "ajax/ajaxdesktop.php",
    data: "opt=1&idjornada=" + idjornada,
    success: function (data) {
      $("#loading").hide();
      $("#lastest_results").html(data);
      init_button_results();
    },
  });
}

function do_set_pref(year, txtyear) {
  $("#loading").show();
  var txtpref = escape(txtyear);
  $.ajax({
    type: "POST",
    url: "ajax/ajaxdesktop.php",
    data: "opt=4&year=" + year + "&txtpref=" + txtpref,
    success: function (data) {
      $("#loading").hide();
      window.location.href = "desktop.php";
    },
  });
}

function do_logout() {
  $("#loading").show();
  $.ajax({
    type: "POST",
    url: "ajax/ajaxdesktop.php",
    data: "opt=3",
    success: function (data) {
      $("#loading").hide();
      window.location.href = "index.php";
    },
  });
}
