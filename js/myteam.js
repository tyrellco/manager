/*
 *      myteam.js
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */



function do_set_orderby (idelement,orden){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmyteam.php",
					data: "opt=1&criterio="+idelement+"&orden="+orden,
					success:function(data) {
										$("#loading").hide();
										$("#leftcontainer").html(data);
										var element="#"+idelement;
										if (orden=="ASC")
											$(element).attr("class","DESC");
										else
											$(element).attr("class","ASC");
											
										$("#byplayer").attr("title","ordernar por "+$("#byplayer").text());
										$("#bygoals").attr("title","goles totales");
										$("#byreds").attr("title","expulsiones totales");
										$("#byinit").attr("title","las veces que este jugador ha sido alineado en la antiliga");
										$("#byplayed").attr("title","las veces que ha jugado realmente con su equipo de la LFP");
										$("#byptos").attr("title","puntos totales");
										$("#leftcontainer.leftmyteam table tr.headdata td").bind("click",function (){
											 do_set_orderby($(this).attr("id"),$(this).attr("class"));
										});
		
									}
				});
}

function do_init_links(){
	$("#byplayer").attr("title","ordernar por "+$("#byplayer").text());
	$("#bygoals").attr("title","goles totales");
	$("#byreds").attr("title","expulsiones totales");
	$("#byinit").attr("title","las veces que este jugador ha sido alineado en la antiliga");
	$("#byplayed").attr("title","las veces que ha jugado realmente con su equipo de la LFP");
	$("#byptos").attr("title","puntos totales");
	$("#leftcontainer.leftmyteam table tr.headdata td").bind("click",function (){
		 do_set_orderby($(this).attr("id"),$(this).attr("class"));
	});
	$("#leftcontainer.leftmyteam table tr td.delplayer").bind("click",function (){
		if (confirm("Confirma que deseas eliminar a '"+$(this).siblings(".playername").text()+"' de tu plantilla"))
		 do_down_player($(this).attr("id"),$(this).siblings(".playername").text());
		else
			alert("Rajao");
	});
	$("#leftcontainer.leftmyteam table tr td.addplayer").bind("click",function (){
		 do_get_form_addplayer();
	});
}

function do_down_player(idplayer,playername){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmyteam.php",
					data: "opt=2&idplayer="+idplayer+"&playername="+playername,
					success:function(data) {
										$("#loading").hide();
										$("#leftcontainer").html(data);
										do_init_links();
									}
				});
}

function do_get_form_addplayer(){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmyteam.php",
					data: "opt=3",
					success:function(data) {
										$("#loading").hide();
										$("#popup").html(data);
										$("#popup").show();
										$("#popup select#idplayer").attr("disabled",true);
										$("#popup select#codlfp").change(function () {
											do_filter_players($(this).val());
										});
										$("#popup input#btnaccept").click(function () {
											if ($("#popup select#idplayer").val()!=0){
											  var textoption=$("#popup select#idplayer option:selected").text();
											  var playername=textoption.substring(0,textoption.indexOf(" - "));
												do_assign_player($("#popup select#idplayer").val(),playername);
											}	
											else
												alert("Selecciona un jugador, inútil");	
										});
										$("#popup input#btncancel").click(function () {
											$("#popup").empty();
											$("#popup").hide();
										});
									}
					});
			
}

function do_filter_players(codlfp){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmyteam.php",
					data: "opt=4&codlfp="+codlfp,
					success:function(data) {
										$("#loading").hide();
										$("#popup select#idplayer").attr("disabled",false);
										$("#popup select#idplayer").html(data);
									}
					});
			
}

function do_assign_player (idplayer,playername){
  $("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmyteam.php",
					data: "opt=5&idplayer="+idplayer+"&playername="+playername,
					success:function(data) {
										$("#loading").hide();
										$("#popup").empty();
										$("#popup").hide();
										$("#leftcontainer").html(data);
										do_init_links();
									}
					});
}
