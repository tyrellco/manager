/*
 *      myuser.js
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */



function prepare_form(){
	$("#btn_send_form_data_user").bind("click",function (){
			send_data_user();
		});
	$("#form_data_user #passwd").bind("focus",function (){
		$(this).val('');
		$("#form_data_user #rpasswd").val('');
	});
	
}

function send_data_user(){
	if ( $("#form_data_user #passwd").val() != $("#form_data_user #rpasswd").val() ){
			alert("Las constraseñas no coinciden");
			return false;
	}
	
	var regexpattern=/[ñáéíóúÁÉÍÓÚºª]/;
	if ($("#form_data_user #passwd").val().match(regexpattern)){
		alert("La password no puede tener ni eñes ni tildes");
		return false;
	}	
	
	var movil=$("#form_data_user #movil").val().replace(/^\s*|\s*$/g,"");
	if ( isNaN(movil) || (movil.length<=0) ){
		alert("Revisa ese número de movil anda");
		return false;
	}	
	
	$("#loading").show();
	var username=$("#form_data_user #username").val();
	var passwd=$("#form_data_user #passwd").val();
	var movil=$("#form_data_user #movil").val();
	var twitter=$("#form_data_user #twitter").val();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxmyuser.php",
					data: "opt=1&passwd="+passwd+"&movil="+movil+"&twitter="+twitter,
					success:function(data) {
										$("#loading").hide();
										window.location.href="index.php";
									}
				});

}

