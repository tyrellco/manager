/*
 *      manager.js
 *
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function remove_last_eleven() {
  if (confirm("Confirma que deseas eliminar el 11 y volver a empezar")) {
    $("#loading").show();
    $.ajax({
      type: "POST",
      url: "ajax/ajaxmanager.php",
      data: "opt=6",
      success: function (data) {
        window.location.href = "m.manager.php";
      },
    });
  }
}

function set_initial_eleven() {
  $("#loading").show();
  $.ajax({
    type: "POST",
    url: "ajax/ajaxmanager.php",
    data: "opt=5",
    success: function (data) {
      if (data == 0) {
        var mensaje = "Aún no tienes 11 jugadores !!";
        var tipo = "alerta";
        show_message(mensaje, tipo, 3000);
      } else {
        $("#rightcontainer").hide();
        $("#rightcontainer").html(data);
        $("#rightcontainer").removeClass("grass");
        $("#rightcontainer").addClass("tactic");
        $("#rightcontainer").fadeIn(800);
        $("#btn_get_grass").val("aceptar y volver");
        $("#btn_go_home").bind("click", function () {
          window.location.href = "index.php";
        });
      }
      $("#loading").hide();
    },
  });
}

function get_eleven() {
  $("#loading").show();
  $.ajax({
    type: "POST",
    url: "ajax/ajaxmanager.php",
    data: "opt=7",
    success: function (data) {
      $("#rightcontainer").hide();
      $("#rightcontainer").html(data);
      $("#rightcontainer").removeClass("tactic");
      $("#rightcontainer").addClass("grass");
      $("#rightcontainer").fadeIn(800);
      $("#btn_get_grass").val("enviar");
      $("#btn_get_grass").unbind();
      $("#btn_send_data").bind("click", function () {
        set_initial_eleven();
      });
      makes_droppables();
      $("#loading").hide();
    },
  });
}

function get_grass(idtactic) {
  $("#loading").show();
  $.ajax({
    type: "POST",
    url: "ajax/ajaxmanager.php",
    data: "opt=3&idtactic=" + idtactic,
    success: function (data) {
      $("#rightcontainer").hide();
      $("#rightcontainer").html(data);
      $("#rightcontainer").removeClass("tactic");
      $("#rightcontainer").addClass("grass");
      $("#rightcontainer").fadeIn(800);
      $("#btn_get_grass").val("enviar");
      $("#btn_get_grass").unbind();
      $("#btn_send_data").bind("click", function () {
        set_initial_eleven();
      });
      $("#btn_cancel_grass").bind("click", function () {
        if (confirm("Seguro que deseas empezar de nuevo ?"))
          window.location.reload();
      });

      makes_droppables();
      $("#loading").hide();
    },
  });
}

function set_captain(objdragbl, objdropbl) {
  if (confirm($(objdragbl).text() + " de capitán ??")) {
    $("#loading").show();
    $.ajax({
      type: "POST",
      url: "ajax/ajaxmanager.php",
      data: "opt=4&playerid=" + $(objdragbl).attr("id"),
      success: function (data) {
        if (data != 0) {
          $(data + " :p").css("color", "#000");
        }
        var selector = "#" + $(objdropbl).attr("id") + " :p";
        $(selector).css("color", "red");
        $("#loading").hide();
      },
    });
  }
}

function remove_player(objdragbl, objdropbl) {
  if (
    confirm(
      "Confirma que quieres cepillarte a " + $(objdragbl).text() + " del 11"
    )
  ) {
    $("#loading").show();
    $.ajax({
      type: "POST",
      url: "ajax/ajaxmanager.php",
      data: "opt=2&playerid=" + $(objdragbl).attr("id"),
      success: function (data) {
        $(objdropbl).html("");
        $(objdropbl).droppable("enable");
        var iddorsal = "#dorsal_" + $(objdragbl).attr("id");
        $(iddorsal).css("background-color", "#4D4D4D");
        $("#loading").hide();
      },
    });
  }
}

function set_player(objdragbl, objdropbl, equipacion) {
  $("#loading").show();
  $.ajax({
    type: "POST",
    url: "ajax/ajaxmanager.php",
    data:
      "opt=1&playerid=" +
      $(objdragbl).attr("id") +
      "&idpos=" +
      $(objdropbl).attr("id"),
    success: function (data) {
      if (data == 1) {
        $(objdropbl).html("");
        var html = "<img src='" + equipacion + "' border='0'/><p>";
        html += $(objdragbl).html() + "</p>";
        var iddorsal = "#dorsal_" + $(objdragbl).attr("id");
        $(iddorsal).css("background-color", "#FF247A");
        $(iddorsal).css("color", "#FFFFFF");
        $(objdropbl).html(html);
        $(objdropbl).droppable("disable");
        /*$(objdropbl).bind("click", function(e) {
																									 set_captain(objdragbl,objdropbl);
																								});*/
        $(objdropbl).bind("dblclick", function (e) {
          remove_player(objdragbl, objdropbl);
        });
      } else {
        alert($(objdragbl).html() + " otra vez ??, naranjas de la china");
      }
      $("#loading").hide();
    },
  });
}

function do_prepare_eleven_mobi() {
  var total = $(
    "#main_container form#form_team_mobi input:checkbox:checked"
  ).length;
  if (total != 11) alert("No puedes jugar con " + total + " jugadores");
  else {
    var count_por = $(
      "#main_container form#form_team_mobi input.por:checkbox:checked"
    ).length;
    var count_def = $(
      "#main_container form#form_team_mobi input.def:checkbox:checked"
    ).length;
    var count_med = $(
      "#main_container form#form_team_mobi input.med:checkbox:checked"
    ).length;
    var count_del = $(
      "#main_container form#form_team_mobi input.del:checkbox:checked"
    ).length;
    var tactic_mobi = check_tactic_mobi(
      count_por + "#" + count_def + "#" + count_med + "#" + count_del
    );
    if (tactic_mobi == "maaaal") alert("No puedes jugar con esa táctica");
    else {
      var eleven_sausage =
        $(
          "#main_container form#form_team_mobi input.por:checkbox:checked"
        ).attr("id") +
        "#" +
        tactic_mobi +
        "gk";
      var cont = 1;
      //defensas
      $("#main_container form#form_team_mobi input.def:checkbox:checked").each(
        function () {
          eleven_sausage +=
            ";" + $(this).attr("id") + "#" + tactic_mobi + "df" + cont;
          cont++;
        }
      );
      //medios
      cont = 1;
      $("#main_container form#form_team_mobi input.med:checkbox:checked").each(
        function () {
          eleven_sausage +=
            ";" + $(this).attr("id") + "#" + tactic_mobi + "mid" + cont;
          cont++;
        }
      );
      //delanteros
      cont = 1;
      $("#main_container form#form_team_mobi input.del:checkbox:checked").each(
        function () {
          eleven_sausage +=
            ";" + $(this).attr("id") + "#" + tactic_mobi + "forw" + cont;
          cont++;
        }
      );

      do_send_eleven_mobi(eleven_sausage);
    }
  }
}

function do_send_eleven_mobi(eleven_sausage) {
  $.mobile.loading("show");
  $("#main_container form#form_team_mobi a#btn_send_team_mobi").unbind("click");
  $("#main_container form#form_team_mobi a#btn_send_team_mobi").html(
    "procesando ..."
  );
  $.ajax({
    type: "POST",
    url: "ajax/ajaxmanager.php",
    data: "opt=8&sausage=" + eleven_sausage,
    success: function (data) {
      $.mobile.loading("hide");
      window.location.href = "manager.php";
    },
  });
}

function check_tactic_mobi(tactic) {
  switch (tactic) {
    case "1#3#4#3":
      return "_343";
      break;
    case "1#3#5#2":
      return "_352";
      break;
    case "1#4#3#3":
      return "_433";
      break;
    case "1#4#4#2":
      return "_442";
      break;
    case "1#4#5#1":
      return "_451";
      break;
    case "1#5#3#2":
      return "_532";
      break;
    case "1#5#4#1":
      return "_541";
      break;
    case "1#3#3#4":
      return "_334";
      break;
    default:
      return "maaaal";
  }
}

function do_reset_form_mobi() {
  $("#main form#form_team_mobi input:checkbox").each(function () {
    this.checked = false;
  });
}
