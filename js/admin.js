/*
 *      admin.js
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */



function do_get_form_datafile(){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxadmin.php",
					data: "opt=1",
					success:function(data) {
										$("#loading").hide();
										$("#rightcontainer").html(data);
										$("#btn_send_datafile").bind("click",function(){
											if ( ($("#numjornada").val()==0) || ($("#justdatafile").val()==""))
												alert("se te olvida algo");
											else{	
												$("#rightcontainer table tr td#td_btn_send_datafile").html("procesando ...");
												$("#form_datafile").submit();
											}	
										});
									}
				});
}

function do_send_form_allplayers(sausage,idteam,nameteam){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxadmin.php",
					data: "opt=6&idteam="+idteam+"&sausage="+sausage+"&nameteam="+nameteam,
					success:function(data) {
										$("#loading").hide();
										//limpiamos, aún no se porque a veces la respuesta viene con basura
										data=data.replace(/(^\s*)|(\s*$)/g,"");
										alert(data);
										$('#form_allplayers').each (function(){
											this.reset();
										});
									}
				});
}


function do_send_form_faults(faults_sausage){
	$("#loading").show();
	$("#rightcontainer").hide();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxadmin.php",
					data: {opt: 3, sausage: faults_sausage},
					success:function(data) {
										$("#loading").hide();
										$("#rightcontainer").html(data);
										$("#rightcontainer").fadeIn();
									}
				});
}

function do_get_form_faults(){
	$("#loading").show();
	var sausage="";
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxadmin.php",
					data: "opt=2",
					success:function(data) {
										$("#loading").hide();
										$("#rightcontainer").html(data);
										$("#btn_send_faults").bind("click",function(){
											sausage="";
											$("form#form_faults table tr").each(function() {
												if ($(this).attr("id").length > 0){
													var idequipo=$(this).attr("id");
													sausage +=idequipo+";";
													sausage +=$("form#form_faults table tr td input#fault_vuelta1_"+idequipo).val()+";";
													sausage +=$("form#form_faults table tr td input#fault_vuelta2_"+idequipo).val()+";";
													sausage +=$("form#form_faults table tr td input#fault_vuelta3_"+idequipo).val()+"#";
												}
											});
											do_send_form_faults(sausage);
										});
									}
				});
}

function do_get_form_allplayers(){
	$("#loading").show();
	var sausage="";
	var cont=0;
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxadmin.php",
					data: "opt=5",
					success:function(data) {
										$("#loading").hide();
										$("#rightcontainer").html(data);
										$("#send_allplayers").bind("click",function(){
											cont=0;
											sausage="";
											$("input:checked").each(function(){
												cont++;
												sausage +=$(this).val()+"#";	
											})
											if ($("#idteam").val()==0)
												alert("Se te ha olvidado seleccionar el equipo");
											else{
												if ( (cont < 15) || (cont > 22) )
													alert("Recuerda que la plantilla tiene que tener entre 15 y 22 jugadores");
												else
													do_send_form_allplayers(sausage,$("#idteam").val(),$("#idteam option:selected").text())
											}	
										});
									}
				});
}

