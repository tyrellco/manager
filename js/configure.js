/*
 *      configure.js
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */



function set_data_configure(idinput,value){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxconfigure.php",
					data: "opt=1&idinput="+idinput+"&value="+value,
					success:function(data) {
										$("#loading").hide();
										//quitamos los espacios en blanco y retornos de carro, 
										//no se porque aqui concretamente mete esta basura, en otros casos no lo hace
										var hecho=data.replace(/(^\s*)|(\s*$)/g,"");
										if (hecho=="ko"){
											var mensaje="Argggg, ha ocurrido un error al copiar la camiseta, si te parece inténtalo de nuevo";
											show_message(mensaje,"error",5000);
										}
										else
											do_get_summary_data();	
									}
				});
}

function do_get_shirts_list(){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxconfigure.php",
					data: "opt=2",
					success:function(data) {
										$("#rightcontainer").html(data);
										$("#rightcontainer").show(800);
										$("#loading").hide();
										$("#btn_send_shirt").bind("click",function (){
											var selectedshirt=$("#shirtselected").val();
											if (selectedshirt!=0){
												$("#equipacion1").val(selectedshirt+".png");
												$("label[for='equipacion1']").removeAttr("class");
												$("label[for='equipacion1']").addClass("ok");
												$("#shirtselected").val(0);
												set_data_configure("equipacion1",selectedshirt+".png");
												do_get_summary_configure_team();
											}
											else{
												var mensaje="Tienes que seleccionar una camiseta";
												show_message(mensaje,"alerta",3000);
											}	
													
										});
										
										$("#btn_cancel_shirt").bind("click",function (){
											do_get_summary_configure_team();
										});
																			
										$(".sampleshirt").bind("click",
											function (){
													var valueoldselected=$("#shirtselected").val();
													var idoldselected="#"+valueoldselected;
													
													if (valueoldselected != 0){
														$(idoldselected).css("border","1px solid #23ACFF");
													}	
													$("#shirtselected").val($(this).attr("id"));	
													$(this).css("border","2px solid #23ACFF");
											});
									}
				});
}

function do_get_summary_configure_team(){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxconfigure.php",
					data: "opt=3",
					success:function(data) {
										$("#loading").hide();
										$("#rightcontainer").hide();
										$("#rightcontainer").html(data);
										$("#rightcontainer").show();
										$("#btn_comeback").bind("click",function (){ window.location.href="index.php";});
										do_get_summary_data();
									}
				});
}

function check_form_team(){
	var value=0;
	var id="";
	$("#form_team input.required").each(function(){
		value=$(this).val();
		id=$(this).attr("id");	
		//aquí no controlamos los espacios en blanco, porque los datos ya están precargados y fue controlado en su momento
		if (value.length > 0)
			$("label[for='"+id+"']").addClass("ok");
		else
			$("label[for='"+id+"']").addClass("ko");
	});
	$("#form_team input.number").each(function(){
		//quitamos los espacios en blanco al inicio y final de la cadena
		var value=$(this).val().replace(/^\s*|\s*$/g,"");
		var id=$(this).attr("id");	
		// isNaN devuelve false (0) si es un entero
		if ( isNaN(value) || (value.length<=0) )
			$("label[for='"+id+"']").addClass("ko");
		else
			$("label[for='"+id+"']").addClass("ok");
	});
	
}

function do_get_summary_data(){
	var shirt=$("#equipacion1").val();
	var manager2="";
	if ($("#manager2").val()=="")
		manager2="¿?";
	else
		manager2=$("#manager2").val();	
	$("legend#summary_name").css("background-image","url('shirts/"+shirt+"')");
	$("#summary_name").html($("#nombre").val());
	$("#summary_manager1").html($("#manager1").val());
	$("#summary_manager2").html(manager2);
	$("#summary_estadio").html($("#estadio").val());
	$("#summary_campeon").html($("#campeon").val());
	$("#summary_copa").html($("#copa").val());
	$("#summary_cuchara").html($("#cuchara").val());
}
