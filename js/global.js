/*
 *      global.js
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function show_message(mensaje,tipo,time){
	$("#message").addClass(tipo);
	$("#message").text(mensaje);
	$("#message").show("slow");
	setTimeout(function(){$("#message").hide("slow");},time);
}

function check_for_enter_configure (event) {
	//Si pulsamos enter forzamos a perder el foco para que envíe los datos a la BBDD
  if (event.keyCode == 13) {
		this.blur();
  }
}

function check_for_enter_login (event) {
	//Si pulsamos enter forzamos el click del boton para que envie la peticion de login
  if (event.keyCode == 13) {
		$("#loginbtn a").click();
  }
}

function do_get_enemydata(idequipo){
	$("#loading").show();
	$.ajax({
					type: 'POST',
					url: "ajax/ajaxdesktop.php",
					data: "opt=2&idenemy="+idequipo,
					success:function(data) {
										$("#loading").hide();
										$("#popup").html(data);
										$("#popup").show(800);
										$("#closepopup").bind("click",function (){$("#popup").hide(800);});
									}
				});
}
