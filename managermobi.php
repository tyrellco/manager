<?php
/*
 *      managermobi.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/tmp");
session_start();
if ( !isset($_SESSION["userinfo"]) )	header ("Location: login.php");
require_once(dirname(__FILE__)."/conf/config.php");
require_once (dirname(__FILE__)."/includes/manager.inc.php");
unset ($_SESSION["alineacion"]);
unset ($_SESSION["capitan"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>antiliga-manager</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link href="css/site.css" rel="stylesheet" type="text/css">
<link href="css/mobi.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<script language="Javascript" src="libjs/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" src="js/manager.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">
	$(document).ready(function(){
	
	$("#main form#form_team_mobi div table tr td").bind("click",function (){
		if ($(this).next("td").children().is(":checked")){
			$(this).next("td").children().removeAttr("checked");
			$(this).next("td").children().parent().removeClass("dorsalselected");
  		$(this).next("td").children().parent().siblings("td.nameplayer").removeClass("dorsalselected");
		}	
		else{
			$(this).next("td").children().attr("checked","checked");
			$(this).next("td").children().parent().addClass("dorsalselected");	
			$(this).next("td").children().parent().siblings("td.nameplayer").addClass("dorsalselected");
		}	
	});
	
	$("#main form#form_team_mobi input#btn_send_team_mobi").bind("click",function (){
		do_prepare_eleven_mobi();
	});
	$("#main form#form_team_mobi input#btn_reset_form_mobi").bind("click",function (){
		window.location.href="manager.php";
	});
	
	$("#main form#form_team_mobi input:checkbox").bind("click",function (){
		if ($(this).is(':checked'))
		{
  		$(this).parent().addClass("dorsalselected");
  		$(this).parent().siblings("td.nameplayer").addClass("dorsalselected");
		}
		else{
  		$(this).parent().removeClass("dorsalselected");
			$(this).parent().siblings("td.nameplayer").removeClass("dorsalselected");
		}	
	});
	
});
</script>
</head>

<body>
<div id="main">
<?php echo get_team_mobi($_SESSION["userinfo"]["idequipo"]); ?>
</div>
</body>
</html>