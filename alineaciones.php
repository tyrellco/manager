﻿<?php
/*
 *      rss.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

header("Content-type: text/html");
session_save_path(dirname(__FILE__)."/tmp");
session_start();
require_once(dirname(__FILE__)."/conf/config.php");
function get_demarcacion($iddemarc){
	switch ($iddemarc){
		case 1: $demarc="POR";
						break;
		case 2: $demarc="DEF";
						break;
		case 3: $demarc="MED";
						break;
		case 4: $demarc="DEL";
						break;												
	}
	return $demarc;
}

function get_eleven_html(){
	require_once (dirname(__FILE__)."/classes/BBDD.class.php");

	$oBBDD=BBDD::get_instancia();
	$sql="SELECT DISTINCT e.nombre equipo,a.idequipo idequipo,a.jornada jornada FROM `0_equipos` e INNER ";
	$sql .="JOIN `0_alineaciones` a ON a.idequipo=e.id ORDER BY a.id DESC";
	$obj_eleven=$oBBDD->get_resource($sql);
	$html="";
	if (mysqli_num_rows($obj_eleven) > 0){
			while ( $eleven=mysqli_fetch_object($obj_eleven) ){
				$sql ="SELECT `jornadalfp` FROM `0_calendario` WHERE `numjornada`=".$eleven->jornada;
				$obj_player=$oBBDD->get_resource($sql);
				$player=mysqli_fetch_object($obj_player);
				$jornadalfp=$player->jornadalfp;
				$sql="SELECT j.nombre nombre,j.demarcacion dem, a.fecha fecha FROM `0_jugadores` j INNER JOIN `0_alineaciones` a ";
				$sql .="ON a.idjugador=j.id WHERE a.idequipo=".$eleven->idequipo." AND a.jornada=".$eleven->jornada;
				$sql .=" ORDER BY dem";
				$obj_player=$oBBDD->get_resource($sql);
				//saco el primer jugador para pillar la fecha de puesta y hacerla coincidir con la fecha de publicación
				$player=mysqli_fetch_object($obj_player);
				$demarcacion=get_demarcacion($player->dem);
				$pubdate=$player->fecha;
				$firstplayer="<li>".$player->nombre." - ".$demarcacion."</li>";
				$html .=<<<eof
				<h4>Jornada {$eleven->jornada} (Jornada {$jornadalfp} LFP), <strong>{$eleven->equipo}</strong></h4>
				<ol>{$firstplayer}
eof;
				while ( $player=mysqli_fetch_object($obj_player) ){ //sigo con el resto de jugadores
					$demarcacion=get_demarcacion($player->dem);
					$html .="<li>".$player->nombre." - ".$demarcacion."</li>\n";
				}
				$html .=<<<eof
				</ol>
				<div>{$pubdate}</div>\n<hr>\n
eof;
			}
	}
	return $html;		
}

$momento=strftime("%a, %d %b %Y %H:%M:%S %Z");
$htmleleven=get_eleven_html();
$htmldoc=<<<eof
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>alineaciones-manager</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<style type="text/css">
body{font-family:sans-serif;}
h4{ color:#0000FF; }
hr{border: 1px solid #D3D1D1;}
ol,div{ color:#000; font-size:12px}
a img{border: 0px;}
</style>
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
</head>
<body>
<div id="main">
<div><a href="http://antiliga.org"><img src="images/logoappdesktop.png" /></a></div>
<hr>
{$htmleleven}
</div>
</body>
</html>
eof;
echo $htmldoc;
?>
