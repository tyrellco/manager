<?php
/*
 *      manager.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
session_save_path(dirname(__FILE__)."/tmp");
session_start();

if ( !isset($_SESSION["userinfo"]) )	header ("Location: login.php");
if (isset ($_SESSION["year"])){
	if ($_SESSION["year"]!="0") 
		header ("Location: desktop.php");
}
require_once(dirname(__FILE__)."/conf/config.php");
require_once (dirname(__FILE__)."/includes/manager.inc.php");
unset ($_SESSION["alineacion"]);
unset ($_SESSION["capitan"]);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>antiliga-manager</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link href="css/manager.css" rel="stylesheet" type="text/css">
<link href="css/site.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

<script language="Javascript" src="libjs/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" src="js/manager.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">

var ahoramismo = new Date("<?php echo date("d M Y G:i:s");?>");

function clock(){ 
		var hora = ahoramismo.getHours() 
		var minutos = ahoramismo.getMinutes() 
		var segundos = ahoramismo.getSeconds() 
		if (hora <= 9) hora = "0" + hora 
		if (minutos <= 9) minutos = "0" + minutos 
		if (segundos <= 9) segundos = "0" + segundos 
		var reloj = hora + ":" + minutos + ":" + segundos 
		$("#clock").html(reloj);
		ahoramismo.setSeconds(ahoramismo.getSeconds() +1);
		setTimeout("clock()",1000); 
	}	
	
	
 /* Esta función no puede ir en un fichero externo porque lleva PHP y fallaría*/
function makes_droppables(){
		$(".dropblpor").droppable({
			accept: ".por",
			hoverClass: 'dropover',
      drop: function(event, ui) {
				set_player(ui.draggable,this,"<?php echo SHIRTS."/gkshirts/".$_SESSION["team"]["equipaciongk"]?>");
			}
    });
		
		$(".dropbldef").droppable({
			accept: ".def",
			hoverClass: 'dropover',
      drop: function(event, ui) {
				set_player(ui.draggable,this,"<?php echo SHIRTS."/".$_SESSION["team"]["equipacion"]?>");
			}
    });
		
		$(".dropblmed").droppable({
			accept: ".med",
			hoverClass: 'dropover',
      drop: function(event, ui) {
				set_player(ui.draggable,this,"<?php echo SHIRTS."/".$_SESSION["team"]["equipacion"]?>");
			}
    });
		
		$(".dropbldel").droppable({
			accept: ".del",
			hoverClass: 'dropover',
      drop: function(event, ui) {
				set_player(ui.draggable,this,"<?php echo SHIRTS."/".$_SESSION["team"]["equipacion"]?>");
			}
    });
}


$(document).ready(function(){
		$("#popup").hide();
		$("#loading").hide();
		$("#message").hide();
		$("#btn_modify_eleven").bind("click",function (){modify_last_eleven(); });
		$("#btn_go_home").bind("click",function (){ window.location.href="index.php";});
		$("#btn_cancel_tactic").bind("click",function (){ window.location.href="index.php";});
		$("#idtactic").val(0);
		$("#btn_get_grass").bind("click",function (){
																									if ($("#idtactic").val() !=0 )		 
																										get_grass($("#idtactic").val());
																									else{	 
																										var mensaje="Selecciona una táctica anda...";
																										var tipo="alerta";
																										show_message(mensaje,tipo,3000);
																									}	
		});
		
		$(".divtactic").bind("click",function (){
																		if ($("#idtactic").val()!=0){
																			var	idold="#"+$("#idtactic").val();
																			$(idold).removeAttr("style");
																		}	
																		var idnew=$(this).attr("id");
																		$("#idtactic").val(idnew);
																		$(this).css("background-color","#aad9ff");
																		$(this).css("color","#FFF"); 
																		$(this).css("border","1px solid #a2a6a6");
																		$(this).css("cursor","default");
																  });
			$("#nextenemy fieldset div.nameteam").bind("click",function (){
					do_get_enemydata($(this).attr("id"));
			});
																	
		$(".por, .def, .med, .del").draggable({ 
			revert:true,
			drag: function(event, ui) { 
							$(this).css("font-weight","bold");
							$(this).css("color","#FFFFFF");
							$(this).css("border","1px dashed #BFBFBF");
							$(this).css("text-align","center");
						},
			stop: function(event, ui) {
							$(this).css("color","#000");
							$(this).css("border","0px");
							$(this).css("text-align","left");
						}			
		});

	clock();	
	
 });

</script>
</head>

<body>
<div id="main">
<div id="loading">procesando...</div>
<div id='message'></div>
 <div id="fullteam"><?php echo get_team($_SESSION["userinfo"]["idequipo"]); ?></div>
 <div class='div_info' id="alloro">
	<fieldset>
		 <legend>hora del servidor</legend>
		 <div id="clock"></div>
	</fieldset> 
 </div>
 <div class='div_info' id="nextmatch"><?php echo next_match();?></div>
 <div class='div_info' id="nextenemy"><?php echo next_enemy();?></div>
 <div class='div_info' id="lfpmatches"><?php echo get_lfp_matches();?></div>
 <!-- rightcontainer -->
 <div id="rightcontainer" class="tactic">	
	 <?php echo init_manager(); ?>
 </div>
	<!-- FIN rightcontainer -->
	<div id="popup" class="enemydata"></div>
<div>
</body>
</html>
