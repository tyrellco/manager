<?php
/*
 *      ajaxmanager.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/../tmp");
session_start();

require_once (dirname(__FILE__)."/../includes/manager.inc.php");

switch ($_POST["opt"]){
	//agregar jugador al once inicial
	case 1: $playerid=$_POST["playerid"];
					$idpos=$_POST["idpos"];
					if (isset ($_SESSION["alineacion"]) ){
						if (array_key_exists($playerid,$_SESSION["alineacion"]))
							$action=0; // este jugador ya existe,no lo pongo
						else{
							$_SESSION["alineacion"][$playerid]=$idpos;
							$action = 1;
						}	
					}
					else{
						$_SESSION["alineacion"][$playerid]=$idpos;
						$action = 1;
					}
					echo $action;
					break;
	//eliminar un jugador del once inicial				
	case 2: $playerid=$_POST["playerid"];
					unset ($_SESSION["alineacion"][$playerid]);
					echo $playerid;
					break;
	// sembrar el cesped				
	case 3: $idtactic=$_POST["idtactic"];
					echo get_tactica($idtactic);
					break;
	// ponemos un capitán			
	case 4: $playerid=$_POST["playerid"];
					$posid=0;
					if (isset ($_SESSION["capitan"]) ){
						$oldcaptain=$_SESSION["capitan"];
						$posid="#".$_SESSION["alineacion"][$oldcaptain];
					}	
						
					$_SESSION["capitan"]=$playerid;
					//$posid="#".$_SESSION["alineacion"][$playerid];
					echo $posid;
					break;
	// ponemos la alineación				
	case 5: if (isset ($_SESSION["alineacion"])){
						if ( (count ($_SESSION["alineacion"])) < 11 )
							$data=0;
						else		
							$data=set_initial_eleven();
					}
					else
						$data=0;	
					echo $data;
					break;	
	
		// eliminamos la última				
	case 6: echo remove_last_eleven();
					break;		
	case 7: echo get_eleven($_POST["idjornada"]);
					break;				
	case 8: echo prepare_eleven_mobi($_POST["sausage"]);
					break;						
								
}

?>
