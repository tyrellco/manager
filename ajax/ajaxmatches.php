<?php
/*
 *      ajaxmatches.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/../tmp");
session_start();

require_once (dirname(__FILE__)."/../includes/matches.inc.php");

switch ($_POST["opt"]){
	case 1: echo get_report_match($_POST["jornada"]);
					break;
	case 2: echo set_form_numbers($_POST["idpos"]);
					break;
	case 3: echo set_numbers($_POST["goals"],$_POST["reds"],$_POST["idalineacion"],$_POST["idpos"],$_POST["idjugador"]);
					break;
	case 4: echo get_list_goals_and_reds($_POST["jornada"]);
					break;								
								
}

?>
