<?php
/*
 *      ajaxdesktop.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/../tmp");
session_start();

require_once (dirname(__FILE__)."/../includes/desktop.inc.php");

switch ($_POST["opt"]){
	case 1: echo get_lastest_results($_POST["idjornada"]);
					break;
	case 2: echo get_dataenemy($_POST["idenemy"]);
					break;
	case 3: echo logout();
					break;
	case 4: $year=$_POST["year"];
					$txtpref=$_POST["txtpref"];
					echo set_pref($year,$txtpref);
					break;											
								
}

?>
