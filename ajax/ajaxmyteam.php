<?php
/*
 *      ajaxmyteam.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/../tmp");
session_start();

require_once (dirname(__FILE__)."/../includes/myteam.inc.php");

switch ($_POST["opt"]){
	case 1: echo get_myplayers_data($_POST["criterio"],$_POST["orden"]);
					break;
	case 2: echo down_player($_POST["idplayer"],$_POST["playername"]);
					break;
	case 3: echo get_form_addplayer();
					break;
	case 4: echo get_select_players($_POST["codlfp"]);
					break;
	case 5: echo set_assign_player($_POST["idplayer"],$_POST["playername"]);
					break;
								
}

?>
