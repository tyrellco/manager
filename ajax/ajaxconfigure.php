<?php
/*
 *      ajaxconfigure.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/../tmp");
session_start();

require_once (dirname(__FILE__)."/../includes/configure.inc.php");

switch ($_POST["opt"]){
	//agregar o modificar informacion en la tabla equipos.
	case 1: $idinput=$_POST["idinput"];
					$value=$_POST["value"];
					echo set_data_configure($idinput,$value);
					break;
	//tabla de camisetas				
	case 2: echo get_shirts_list();
					break;
	
	case 3: echo get_summary_configure_team();
					break;								
									
}


?>

