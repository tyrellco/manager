<?php
/*
 *      rss.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

header("Content-type: text/xml");
session_save_path(dirname(__FILE__)."/tmp");
session_start();
require_once(dirname(__FILE__)."/conf/config.php");
require_once(dirname(__FILE__)."/includes/rss.inc.php");

if (isset($_GET["feed"])){
	switch ($_GET["feed"]){
		case "clasif":
		case "clasifg": get_clasifications_rss(1);				
		break;
		case "clasifv": get_clasifications_rss(2);				
		break;
		case "clasifr": get_clasifications_rss(3);				
		break;
		case "allclasif":get_all_clasifications_rss();
		break;
		case "last":get_last_results_rss();
		break;
		case "alin":
		default:get_all_eleven();
		break;
		
	}
}
else
	get_all_eleven();

?>
