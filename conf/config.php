<?php
/*
 *      config.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

/***************** BBDD *****************/

//MySQL produccion
// define ("SERVER","127.0.0.1");
// define ("BBDD","manager");
// define ("USER","managerweb");
// define ("PASSWD","xxxxxxxxx");

//MySQL local
define ("SERVER","172.18.0.3"); //docker inspect mariadb_default -> para saber la ip del docker de MySQL
define ("BBDD","manager");
define ("USER","stumpy");
define ("PASSWD","stumpy");

/************ APLICACIÓN *****************/
//Punto de partida de la clasificación
define("GENERAL",0);
define("VUELTA1",0);
define("VUELTA2",0);
define("VUELTA3",0);
define("REGULARIDAD",0);
//prefijo de la temporada actual
define ("PRE",0);
//número máx de jugadores en la plantilla
define ("MAXJUG",22);
/**********************TWITTER*********************************/
define('_CONSUMER_KEY','awSA5Z73V4CZrL3iZWO9WA'); 
define('_CONSUMER_SECRET','ldJQvbG16lJgPyRohjs9s3sxkF1EfDYVYn8VH8iLjY'); 
define('_OAUTH_TOKEN','357375123-O1a7SRXdHAYyoL3pyRRbSjoqSiYkZXXM57KsIkOE'); 
define('_OAUTH_TOKEN_SECRET','rLgfWgo6Chj6yjVaydT5iesNtuYiY8BUcGF9fHpW4');

/***************** LOG (MoseLog)*****************/
define ("LOGPATH","var/log");//ruta relativa del directorio de log
define ("LOGSEPARATOR"," ");//separador entre los campos del fichero de log
define ("MULTILOG",FALSE);//un sólo fichero de log o un fichero por cada tipo (INFO,WARNING,NOTICE,ERROR)
define ("PREFIX","manager");//prefijo de los ficheros de log

/***************** SMS (smspubli)*****************/
define("SMSUSERNAME","antiliga");
define("SMSPASSWD","2014antiliga");
define("SMSSOURCE","ANTILIGA");
define("USERREFERENCE","JIE32JDGW3223");
define("DELIVERYRECEIPTS","1");

/***************** GENERAL *****************/
//locales
setlocale(LC_TIME, 'es_ES');
//version aplicación
define ("VERSION","v1.3");
//ruta relativa de la camisetas ya seleccionadas
define("SHIRTS","shirts");
//ruta relativa del listado completo de camisetas
define ("ALLSHIRTS","allshirts"); //ruta absoluta de la aplicacion
//ruta relativa y nombre de fichero con los datos iniciales de las clasificaciones
define ("INITFILE","conf/reset_clasif");
?>
