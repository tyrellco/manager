<?php
/*
 *      admin.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");


function update_data_match($ptosjornada,$numjornada){
	$oBBDD=BBDD::get_instancia();
	$total=count($ptosjornada);
	$cont=0;

	foreach($ptosjornada as $idjugador =>$ptos){
		if (trim($ptos)=="-"){ // no ha jugado el partido, no confundir con la rayita de LFM, esa rayita es cosecha propia
			$sql="UPDATE `".get_pref()."_alineaciones` SET `puntos`=0, `jugado`=0 WHERE `idjugador`=".$idjugador;
			$sql .=" AND `jornada`=".$numjornada;
		}	
		else
			$sql="UPDATE `".get_pref()."_alineaciones` SET `puntos`=".$ptos." WHERE `idjugador`=".$idjugador." AND `jornada`=".$numjornada;	
			
		$oBBDD->set_resultados($sql);
	}
}

/*
 * Actualizamos los datos del jugador una vez procesado los datos del partido.
 * */
function update_data_players($ptostotal,$nombres,$vdemarcacion,$vgoles,$vlfp){
	$oBBDD=BBDD::get_instancia();
	$total=count($ptostotal);
	$cont=0;

	foreach($ptostotal as $idjugador =>$ptos){
		$sql="SELECT count(id) hay FROM `".get_pref()."_jugadores` WHERE `id`=".$idjugador;
		$obj_player=$oBBDD->get_resource($sql);
		$player=mysqli_fetch_object($obj_player);
		if ($player->hay > 0){ // El jugador ya existe en la BBDD, continuamos actualizando los datos
			$sql="SELECT count(*) jugado FROM `".get_pref()."_alineaciones` WHERE `jugado`=1 and `idjugador`=".$idjugador;
			$obj_player=$oBBDD->get_resource($sql);
			$player=mysqli_fetch_object($obj_player);
			$matchesplayed=$player->jugado; // Partidos jugados por este jugador
			
			$sql="SELECT count(*) titular FROM `".get_pref()."_alineaciones` WHERE `idjugador`=".$idjugador;
			$obj_player=$oBBDD->get_resource($sql);
			$player=mysqli_fetch_object($obj_player);
			$matchesin=$player->titular; // Partidos alineados de este jugador (puede que luego haya jugado o no)
			
			if (trim($ptos)=="-") // aún no ha jugado
				$ptos=0;
				
			$sql="UPDATE `".get_pref()."_jugadores` SET `puntos`=".$ptos.", `titular`=".$matchesin.", `jugados`=".$matchesplayed;
			$sql .=", `goles`= ".$vgoles[$idjugador]." WHERE `id`=".$idjugador;
			$oBBDD->set_resultados($sql);
		}
		else{ // el jugador no existe en la BBDD, le doy de alta
			if (trim($ptos)=="-") // aún ha jugado
				$ptos=0;
			$sql="INSERT INTO `".get_pref()."_jugadores` SET `puntos`=".$ptos.", `nombre`='".$nombres[$idjugador]."', `id`=".$idjugador;
			$sql .=", idprop=0, `demarcacion`=".$vdemarcacion[$idjugador].", `equipolfp`='".trim($vlfp[$idjugador])."'";
			$oBBDD->set_resultados($sql);
		}
	}
}

function reset_clasifications(){
	$oBBDD=BBDD::get_instancia();
	$handle = fopen(INITFILE,"r");
	//saco la primera línea que es el encabezado y no me vale para nada
	$line= fgets($handle,1024); 
	while ($line= fgets($handle,1024)){
		list($idequipo,$gral,$vta1,$vta2,$vta3,$regraw)=explode("#",$line);
		$reg=str_replace(",",".",$regraw);
		$sql="UPDATE `".get_pref()."_clasificacion` SET `general`=".$gral.", `vuelta1`=".$vta1.", `vuelta2`=".$vta2;
		$sql .=", `vuelta3`=".$vta3.", `regularidad`=".$reg." WHERE `idequipo`=".$idequipo;
		$oBBDD->set_resultados($sql);
	}
	fclose($handle);
	
}	

function update_all_clasifications(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `numjornada` jornada FROM `".get_pref()."_calendario` WHERE `procesado`=1 ORDER BY `numjornada` ASC";
	$obj_matches=$oBBDD->get_resource($sql);
	$sql="SELECT `id` FROM `".get_pref()."_equipos` ORDER BY `id` ASC";
	$obj_teams=$oBBDD->get_resource($sql);
	$vppjornada=array();
	$vptotales=array();

	$total=mysqli_num_rows($obj_teams);
	//Llenamos dos vectores con los puntos totales y los puntos por equipo y jornada
	while ($team=mysqli_fetch_object($obj_teams)){ // equipos
		$ptototal=0;
		while ($match=mysqli_fetch_object($obj_matches)){ //jornadas
			$sql="SELECT SUM(puntos) ptosmatch FROM `".get_pref()."_alineaciones` WHERE `jornada`=".$match->jornada;
			$sql .=" AND `idequipo`=".$team->id;
			$obj_ppjornada=$oBBDD->get_resource($sql);
			$ppjornada=mysqli_fetch_object($obj_ppjornada);
			$vppjornada[$team->id][$match->jornada]=$ppjornada->ptosmatch;
			$ptototal +=$ppjornada->ptosmatch;
		}
		$vptotales[$team->id]=$ptototal;
		mysqli_data_seek($obj_matches,0); //rebobinamos el puntero al principio para la siguiente iteración del while
	}
	
	//Inicializamos todas las clasificaciones
	reset_clasifications();

	mysqli_data_seek($obj_matches,0);
	mysqli_data_seek($obj_teams,0);
	// Actualizamos la clasificación con los datos de los vectores
	while ($team=mysqli_fetch_object($obj_teams)){ // equipos
		while ($match=mysqli_fetch_object($obj_matches)){ //jornadas
			$win=0;
			$sql="SELECT * FROM `".get_pref()."_enfrentamientos` WHERE (`idequipo1`=".$team->id." OR `idequipo2`=".$team->id.")";
			$sql .=" AND `numjornada`=".$match->jornada;
			$obj_data=$oBBDD->get_resource($sql);
			$data=mysqli_fetch_object($obj_data);
			$idenemy=$data->idequipo1;
			if ($idenemy==$team->id)
				$idenemy=$data->idequipo2;
			//Ahora en idenemy tenemos el rival para la jornada en cuestión
			$sql="SELECT `vuelta` FROM `".get_pref()."_calendario` WHERE `numjornada`=".$match->jornada;
			$obj_data=$oBBDD->get_resource($sql);
			$data=mysqli_fetch_object($obj_data); //la vuelta que se corresponde con la jornada en cuestión
			if ($vppjornada[$team->id][$match->jornada] == $vppjornada[$idenemy][$match->jornada])
				$win=1;
			else{
				if ($vppjornada[$team->id][$match->jornada] > $vppjornada[$idenemy][$match->jornada])
					$win=3;
			}	
			if ($win!=0){
				$sql="UPDATE `".get_pref()."_clasificacion` SET `general`=`general`+".$win.", `vuelta".$data->vuelta."`=`vuelta".$data->vuelta."`+".$win;
				$sql .=" WHERE `idequipo`=".$team->id;
				$oBBDD->set_resultados($sql);
			}
				
		}
		$sql="UPDATE `".get_pref()."_clasificacion` SET `regularidad`=`regularidad`+".$vptotales[$team->id]." WHERE `idequipo`=".$team->id;
		$oBBDD->set_resultados($sql);
		mysqli_data_seek($obj_matches,0);
	}
}

function set_faults(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `faults` WHERE `general` > 0";
	$obj_faults=$oBBDD->get_resource($sql);
	while ($fault=mysqli_fetch_object($obj_faults)){
		$sql="UPDATE `".get_pref()."_clasificacion` SET `general`=`general`-".$fault->general.", `vuelta1`=`vuelta1`-".$fault->vuelta1;
		$sql .=", `vuelta2`=`vuelta2`-".$fault->vuelta2.", `vuelta3`=`vuelta3`-".$fault->vuelta3." WHERE `idequipo`=".$fault->idequipo;
		$oBBDD->set_resultados($sql);
	}
}

function process_datafile(){
	if ( ($_SERVER['REQUEST_METHOD'] == 'POST') && (isset ($_POST['wm_matches'])) ){
		$oBBDD=BBDD::get_instancia();
		if (is_uploaded_file($_FILES['justdatafile']['tmp_name'])) {
		$html=<<<eof
		<fieldset>
			<legend class="rotulo">procesando fichero de datos</legend>
				<ul>
				<li id="loadfile">* volcado de fichero: <span class='ok'>OK</span></li>
				<li id="updatedata">* actualizado datos de jornada: <span class='ok'>OK</span></li>
				<li id="updateplayers">* actualizado puntos totales: <span class='ok'>OK</span></li>
				<li id="updateclasif">* actualizado clasificaciones: <span class='ok'>OK</span></li>
				<ul>
				<div class='genericmsg'>Todo correcto, fin del proceso</div>
		</fieldset>	
eof;
			$ptosjornada=array();
			$ptostotal=array();	
			$nombres=array();
			$vdemarcacion=array();
			$vgoles=array();
			$vlfp=array();
			$handle = fopen($_FILES['justdatafile']['tmp_name'],"r");
			while ($line= fgets($handle,1024)){
					list($idjugador,$nombre,$lfp,$ptos,$total,$goles,$iddem)=explode("#",$line);
					$ptos_formateado=str_replace(",",".",$ptos);
					$total_formateado=str_replace(",",".",$total);
					$nombres[$idjugador]=trim($nombre);
					$ptosjornada[$idjugador]=$ptos_formateado;
					$ptostotal[$idjugador]=$total_formateado;
					$vdemarcacion[$idjugador]=$iddem;
					$vgoles[$idjugador]=$goles;
					$vlfp[$idjugador]=$lfp;
			}
		}
		fclose($handle);
		update_data_match($ptosjornada,$_POST['numjornada']);
		update_data_players($ptostotal,$nombres,$vdemarcacion,$vgoles,$vlfp);
		$sql="UPDATE `".get_pref()."_calendario` SET `procesado`=1 WHERE `numjornada`=".$_POST['numjornada'];
		$oBBDD->set_resultados($sql);
		$momento=strftime("%a, %b %d %Y %H:%M:%S %Z");
		$sql="INSERT INTO `0_logupdates` SET `user`='".$_SESSION["userinfo"]["username"]."', ";
		$sql .="`fecha`='".$momento."'";
		$oBBDD->set_resultados($sql);
		update_all_clasifications();
		set_faults();
		if (isset($_POST['sendtwit'])){ //Enviamos el twit ??
			$twit="Atención chicas !!!, la clasificación para la jornada ".$_POST['numjornada']." acaba de ser actualizada por ".$_SESSION["userinfo"]["username"];
			send_twitter_msg($twit);
		}
		
		echo $html;
	}		
	else
		echo "";
}

function get_menu_admin(){
	if (is_admin()){
		$html=<<<eof
		<fieldset>
		<legend class="rotulo">operaciones</legend>
			<ul>
				<li id="op1" class="opadmin">cargar fichero de puntos</li>
				<li id="op2" class="opadmin">sanciones</li>
				<li id="op3" class="opadmin">administrar plantillas</li>
			<ul>
		</fieldset>
eof;
	}
	else{
		$html=<<<eof
		<h2>Creo que no deber&iacute;as estar aqu&iacute;</h2>
eof;
	}	
return $html;	
}


function get_select_old_matches(){
	$oBBDD=BBDD::get_instancia();
	$now=time();
	$html="";
	//solo sacamos aquellas jornadas que se han disputado y que se han jugado tb en la antiliga (mostrar!=0)
	$sql="SELECT `numjornada`,`jornadalfp`,`fecha` FROM `".get_pref()."_calendario` WHERE `fechaunix` < ".$now." AND `mostrar`<>0";
	$obj_calendar=$oBBDD->get_resource($sql);
	$html .="<option value='0' selected> selecciona jornada </option>";
	while ($calendar=mysqli_fetch_object($obj_calendar)){
		$html .="<option value=".$calendar->numjornada.">".$calendar->numjornada."&ordf; jornada - ";
		$html .=$calendar->jornadalfp."&ordf jornada LFP - (".$calendar->fecha.")</option>";
	}	
	return $html;	
}

function get_form_datafile(){
	if (is_admin()){
		$html_old_matches=get_select_old_matches();
		$html=<<<eof
		<fieldset>
		<legend class="rotulo">dame el fichero</legend>
			<form id="form_datafile" name="form_datafile" action="admin.php" method="POST" enctype="multipart/form-data">
			<input type="hidden" id="wm_matches" name="wm_matches" value=""/>
			<table>
				<tr>
					<td class="fileinput"><input type="file" id="justdatafile" name="justdatafile" value=""></td>
					<td class="fileinput">
						<select id='numjornada' name='numjornada'>
							{$html_old_matches}
						</select>
					</td>
					</tr>
					<tr>
					<td colspan="2" class="fileinput"><input type="checkbox" name="sendtwit" id="sendtwit" value="twitter" checked><span>publicar en twitter</span></td>
					</tr>
					<tr>
					<td colspan="2" class="tdbig" id='td_btn_send_datafile'><input type="button" id="btn_send_datafile" name="btn_send_datafile" value="enviar" class="button"/></td>
					</tr>
				</table>		
			</form>
		</fieldset>
eof;
	}
	else{
		$html=<<<eof
		<h2>Creo que no deber&iacute;as estar aqu&iacute;</h2>
eof;
	}	
return $html;	
}

function get_select_teams(){
	$oBBDD=BBDD::get_instancia();
	$html="";
	//solo sacamos aquellas jornadas que se han disputado 
	$sql="SELECT `id`,`nombre` FROM `".get_pref()."_equipos` ORDER BY `nombre`";
	$obj_team=$oBBDD->get_resource($sql);
	$html .="<option value='0' selected> selecciona equipo </option>";
	while ($team=mysqli_fetch_object($obj_team))
		$html .="<option value=".$team->id.">".$team->nombre."</option>";
	return $html;	
}

function get_all_players(){
	$oBBDD=BBDD::get_instancia();
	$html="";
	
	$sql="SELECT `id`,`nombre`,`demarcacion`,`equipolfp` FROM `".get_pref()."_jugadores` ORDER BY `demarcacion`,`nombre`";
	$obj_player=$oBBDD->get_resource($sql);
	$html="";
	$class="";
	$lastdem=1;
	while ($player=mysqli_fetch_object($obj_player)){
		if ($class=="")
			$class="class='painted'";
		else
			$class="";
		if ($lastdem!=$player->demarcacion){
			$html .="<tr class='separator'><td colspan='5'>&nbsp;</td></tr>";
			$lastdem=$player->demarcacion;
		}	
		$html .="<tr ".$class."><td><input type='checkbox' value=".$player->id." /></td><td class='nameplayer'>".$player->nombre."</td>";
		$html .="<td>".strtoupper(get_demarcacion($player->demarcacion))."</td><td>".$player->equipolfp."</td>";
		$html .="<td>".$player->id."</td></tr>";
	}	
		
	return $html;	
}

function get_form_allplayers(){
	if (is_admin()){
		$html_select_teams=get_select_teams();
		$html_all_players=get_all_players();
		$html=<<<eof
		<fieldset>
		<legend class="rotulo">todos los jugadores</legend>
			<form id="form_allplayers" name="form_allplayers" action="admin.php" method="POST">
			<table>
				<tr>
					<td>
						<select id='idteam' name='idteam'>
							{$html_select_teams}
						</select>
					</td>
					<td><input class="button" type="button" id="send_allplayers" name="send_allplayers" value="enviar todo" /></td>
					</tr>
				</table>
				<div id="list_allplayers">
				<table class="allplayers">
				<tr class='head'><td>-</td><td>JUGADOR</td><td>DEM</td><td>LFP</td><td>COD</td></tr>
				{$html_all_players}
				</table>
				</div>	
			</form>
		</fieldset>
eof;
	}
	else{
		$html=<<<eof
		<h2>Creo que no deber&iacute;as estar aqu&iacute;</h2>
eof;
	}	
return $html;	
}

function send_form_allplayers($sausage,$idteam,$nameteam){
	$result="";
	$cont=0;
	if (is_admin()){
		$oBBDD=BBDD::get_instancia();
		// vacio los jugadores para el equipo en cuestión
		$sql="UPDATE `".get_pref()."_jugadores` SET `idprop`=0 WHERE `idprop`=".$idteam;
		$oBBDD->set_resultados($sql);
		$sausage=substr($sausage,0,strlen($sausage)-1); //quitamos la última almohadilla
		$vplayers=explode("#",$sausage);
		foreach ($vplayers as $code){
			$cont++;
			$sql="UPDATE `".get_pref()."_jugadores` SET `idprop`=".$idteam." WHERE `id`=".$code;
			$oBBDD->set_resultados($sql);
		}
		$result="Ok, plantilla de ".$nameteam." salvada de forma correcta, (".$cont. " jugadores)";	
	}
	else
		$result="Creo que no deber&iacute;as estar aqu&iacute";

return $result;

}

function get_all_faults(){
	$oBBDD=BBDD::get_instancia();
	$class="";
	
	$sql="SELECT e.nombre nombre, f.idequipo idequipo, f.general general, f.vuelta1 vuelta1, f.vuelta2 vuelta2, f.vuelta3 vuelta3 ";
	$sql .="FROM `faults` f INNER JOIN `0_equipos` e ON (e.id=f.idequipo) ORDER BY nombre";

	$obj_faults=$oBBDD->get_resource($sql);
	$html="";
	
	while ($fault=mysqli_fetch_object($obj_faults)){
		$html .=<<<eof
		<tr id="$fault->idequipo">
			<td class="teamname">{$fault->nombre}</td>
			<td><input type="text" id="fault_general_{$fault->idequipo}" value="{$fault->general}" disabled/></td>
			<td><input type="text" id="fault_vuelta1_{$fault->idequipo}" value="{$fault->vuelta1}"/></td>
			<td><input type="text" id="fault_vuelta2_{$fault->idequipo}" value="{$fault->vuelta2}"/></td>
			<td><input type="text" id="fault_vuelta3_{$fault->idequipo}" value="{$fault->vuelta3}"/></td>
		</tr>
eof;
	}	
		
	return $html;	
}


function get_all_faults_table(){
	$oBBDD=BBDD::get_instancia();
	$class="";
	
	$sql="SELECT e.nombre nombre, f.idequipo idequipo, f.general general, f.vuelta1 vuelta1, f.vuelta2 vuelta2, f.vuelta3 vuelta3 ";
	$sql .="FROM `faults` f INNER JOIN `0_equipos` e ON (e.id=f.idequipo) ORDER BY nombre";

	$obj_faults=$oBBDD->get_resource($sql);
	$html=<<<eof
	<fieldset>
		<legend class="rotulo">sanciones</legend>
			<form id="form_faults" name="form_faults">
			<table id="datalist">
				<tr>
					<th>equipo</td>
					<th>general</td>
					<th>vuelta 1</td>
					<th>vuelta 2</td>
					<th>vuelta 3</td>
				</tr>
eof;
	while ($fault=mysqli_fetch_object($obj_faults)){
		$html .=<<<eof
		<tr>
			<td class="teamname">{$fault->nombre}</td>
			<td>{$fault->general}</td>
			<td>{$fault->vuelta1}</td>
			<td>{$fault->vuelta2}</td>
			<td>{$fault->vuelta3}</td>
		</tr>
eof;
	}
	
	$html .=<<<eof
	</table>
	</fieldset>
eof;

	return $html;	
}

function get_form_faults(){
	if (is_admin()){
		$html_all_faults=get_all_faults();
		$html=<<<eof
		<fieldset>
		<legend class="rotulo">sanciones</legend>
			<form id="form_faults" name="form_faults">
			<table id="datalist">
				<tr>
					<th>equipo</td>
					<th>general</td>
					<th>vuelta 1</td>
					<th>vuelta 2</td>
					<th>vuelta 3</td>
				</tr>
				{$html_all_faults}
				<tr>
					<td colspan="5" class="tdbig" id='td_btn_send_faults'>
						<input type="button" id="btn_send_faults" name="btn_send_faults" value="enviar" class="button"/>
					</td>
				</tr>
			</table>
			</form>
		</fieldset>
eof;
	}
	else{
		$html=<<<eof
		<h2>Creo que no deber&iacute;as estar aqu&iacute;</h2>
eof;
	}	
return $html;	
}

function send_form_faults($sausage){
	$oBBDD=BBDD::get_instancia();
	$vsausage=array();
	$vsausage4team=array();
	$vvalues=array();
	$vsausage=explode("#",$sausage);
	foreach ($vsausage as $vsausage4team){
		$vvalues=explode(";",$vsausage4team);
		$total=$vvalues[1]+$vvalues[2]+$vvalues[3];
		$sql="UPDATE `faults` SET `general`=".$total.", `vuelta1`=".$vvalues[1].", `vuelta2`=".$vvalues[2].", `vuelta3`=".$vvalues[3];
		$sql .=" WHERE `idequipo`=".$vvalues[0].";";
		$oBBDD->set_resultados($sql);
	}
	echo get_all_faults_table();
}
?>
