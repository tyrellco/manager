<?php
/*
 *      libglogin.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once(dirname(__FILE__)."/../classes/BBDD.class.php");


function login($user,$passwd){
	unset($_SESSION["userinfo"]);
	$oBBDD=BBDD::get_instancia();
	$sha1_passwd=sha1($passwd);
	$sql="SELECT * FROM `users` WHERE `usuario`='".$user."' AND `password`='".$sha1_passwd."'";

	$obj_user=$oBBDD->get_resource($sql);
	$login=1;
	if (mysqli_num_rows($obj_user) == 1){
		$user=mysqli_fetch_object($obj_user);
		$_SESSION["userinfo"]["username"]=$user->usuario;
		$_SESSION["userinfo"]["last"]=$user->last;
		$_SESSION["userinfo"]["rol"]=$user->rol;
		$_SESSION["userinfo"]["idequipo"]=$user->idequipo;
		$_SESSION["userinfo"]["idusuario"]=$user->id;
		$momento=strftime("%a %b %d %Y %H:%M:%S %Z");
		$sql="UPDATE `users` SET `last`='".$momento."' WHERE `id`=".$user->id;
		$oBBDD->set_resultados($sql);
		$login=0;
	}
	return $login;
}
?>
 
