<?php
/*
 *      myteam.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");



function get_myplayers_data($criterio,$orden){
	$oBBDD=BBDD::get_instancia();
	$id=$_SESSION["team"]["idequipo"];
	switch ($criterio){
		case "byplayer": 
					$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `demarcacion` ASC, `nombre` ".$orden;
					break;
		case "bygoals":
					$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `goles` ".$orden;
					break;
		case "byreds":
					$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `rojas` ".$orden;
					break;
		case "byinit":
					$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `titular` ".$orden;
					break;
		case "byplayed":
					$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `jugados` ".$orden;
					break;
		case "bylfp":
					$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `equipolfp` ".$orden;
					break;			
		case "byptos":
					$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `puntos` ".$orden;
					break;
		case "byprice":
		$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$id." ORDER BY `coste` ".$orden;
		break;
	}	

	$obj_team=$oBBDD->get_resource($sql);
	$cont=1;
	$shirt=SHIRTS."/".$_SESSION["team"]["equipacion"];
	$html=<<<eof
	<table>
	<tr class="headdata">
		<td id="byplayer" class="ASC" colspan="2">jugador</td>
		<td id="bygoals" class="DESC">goles</td>
		<td id="byreds" class="DESC">rojas</td>
		<td id="byinit" class="DESC">titular</td>
		<td id="byplayed" class="DESC">jugados</td>
		<td id="byptos" class="DESC">puntos</td>
		<td id="bylfp" class="DESC">lfp</td>
		<td id="byprice" class="DESC">coste</td>
		<td>acción</td>
	</tr>
eof;
	
	while ($player=mysqli_fetch_object($obj_team)){
		$demarc=get_demarcacion($player->demarcacion);
		$html .="<tr class='data".$demarc."'><td class='dorsal'>".$cont."</td>";
		if ($demarc=="por")
			$html .="<td class='playername' style=\"background-image:url('".SHIRTS."/gkshirts/".$_SESSION["team"]["equipaciongk"]."');\">";
		else
			$html .="<td class='playername' style=\"background-image:url('".SHIRTS."/".$_SESSION["team"]["equipacion"]."');\">";
		$html .=$player->nombre."</td>\n";
		$html .="<td class='onlydata'>".$player->goles."</td>\n";
		$html .="<td class='onlydata'>".$player->rojas."</td>\n";
		$html .="<td class='onlydata'>".$player->titular."</td>\n";
		$html .="<td class='onlydata'>".$player->jugados."</td>\n";
		$html .="<td class='onlydata'>".$player->puntos."</td>\n";
		$html .="<td class='onlydata'>".$player->equipolfp."</td>\n";
		$html .="<td class='onlydata'>".$player->coste."</td>\n";
		$html .="<td class='delplayer' id=".$player->id." title='eliminar este jugador'></td><tr/>\n";
		$cont++;
	}
	while ($cont<=MAXJUG){
		$html .="<tr class='dataempty'><td class='dorsal'>".$cont."</td>";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='onlydata'></td>\n";
		$html .="<td class='addplayer' title='añadir un jugador'></td><tr/>\n";
		$cont++;
	}
	$html .="</table>\n";
	
	return $html;

}

function down_player ($idplayer,$playername){
	$oBBDD=BBDD::get_instancia();
	$sql="UPDATE `".get_pref()."_jugadores` SET `idprop`=0 WHERE `id`=".$idplayer;
	$oBBDD->set_resultados($sql);
	$log=new MoseLog();
	$log->info($_SESSION["userinfo"]["username"]." MYTEAM DOWN $playername");
	return get_myplayers_data("byplayer","ASC");
}

function get_select_lfpteams(){
	$oBBDD=BBDD::get_instancia();
	$html="";
	$sql="SELECT `codigo`,`nombre` FROM `".get_pref()."_equiposlfp` ORDER BY `nombre`";
	$obj_teamlfp=$oBBDD->get_resource($sql);
	$html .="<option value='0' selected> selecciona equipo </option>";
	while ($team=mysqli_fetch_object($obj_teamlfp))
		$html .="<option value=".$team->codigo.">".$team->nombre."</option>";
	return $html;	
}

function get_select_players($codlfp){
	$oBBDD=BBDD::get_instancia();
	$html="";
	$sql="SELECT `id`, `idprop`, `nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `equipolfp`='".$codlfp."' and `idprop`=0 ORDER BY `demarcacion`,`nombre`";
	$obj_player=$oBBDD->get_resource($sql);
	$html .="<option value='0' selected> selecciona jugador </option>";
	while ($player=mysqli_fetch_object($obj_player)){
		$dem=strtoupper(get_demarcacion($player->demarcacion));
		$html .="<option value=".$player->id.">".$player->nombre." - ".$dem."</option>";
	}	
	return $html;	
}

function get_form_addplayer(){
		$html_select_lfpteams=get_select_lfpteams();
		$html=<<<eof
		<fieldset>
		<legend class="rotulo">agregar jugador</legend>
			<form id="form_newplayer" name="form_newplayer" action="#" method="POST">
			<table class='table_addplayer'>
				<tr>
					<td>
						<select id='codlfp' name='codlfp'>
							{$html_select_lfpteams}
						</select>
					</td>
					<td>
						<select id='idplayer' name='idplayer'>
							<option value='0' selected> selecciona jugador </option>
						</select>
					</td>
					<td>
					<input type="button" id="btnaccept" name="btnaccept" class="accept" value="aceptar"/>
					</td>
					<td>
					<input type="button" id="btncancel" name="btncancel" class="cancel" value="cancelar"/>
					</td>
				</tr>
				</table>
			</form>
		</fieldset>
eof;

return $html;
}

function set_assign_player ($idplayer,$playername){
	$oBBDD=BBDD::get_instancia();
	$sql="UPDATE `".get_pref()."_jugadores` SET `idprop`=".$_SESSION['team']['idequipo']." WHERE `id`=".$idplayer;
	$oBBDD->set_resultados($sql);
	$log=new MoseLog();
	$log->info($_SESSION["userinfo"]["username"]." MYTEAM ADD $playername");
	return get_myplayers_data("byplayer","ASC");
}

?>


