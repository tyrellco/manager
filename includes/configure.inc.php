<?php
/*
 *      configure.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");

function get_form_team(){
	$oBBDD=BBDD::get_instancia();
	$myid=$_SESSION["team"]["idequipo"];
	$sql="SELECT * FROM `".get_pref()."_equipos` WHERE id=".$myid;
	$obj_team=$oBBDD->get_resource($sql);
	$team=mysqli_fetch_object($obj_team);
	$html=<<<eof
	<div id="div_form_team">
	<form action="#" id="form_team" name="form_team" method="post">
	<input type="hidden" name="shirtselected" id="shirtselected" value="0" />
	<fieldset>
		 <legend>configura tu equipo</legend>
		 <table>
		 <tr><td>nombre del equipo:</td><td><input type="text" id="nombre" value="{$team->nombre}" class="required"/></td>
		 <td><label for="nombre"></label></td>
		 </tr>
		 <tr><td>primer entrenador:</td><td><input type="text" id="manager1" value="{$team->manager1}" class="required"/></td>
		 <td><label for="manager1"></label></td>
		 </tr>
		 <tr><td>2&ordm; entrenador:</td><td><input type="text" id="manager2" value="{$team->manager2}" class="optional"/></td>
		 <td>*</td>
		 </tr>
		 <tr><td>estadio:</td><td><input type="text" id="estadio" value="{$team->estadio}" class="required"/></td>
		 <td><label for="estadio"></label></td>
		 </tr>
		 <tr><td>equipaci&oacute;n:</td><td><input type="text" id="equipacion1" value="{$team->equipacion1}" class="required"/></td>
		 <td><label for="equipacion1"></label></td>
		 </tr>
		 <tr><td>campeonatos:</td><td><input type="text" id="campeon" value="{$team->campeon}" class="number"/></td>
		 <td><label for="campeon"></label></td>
		 </tr>
		 <tr><td>copas:</td><td><input type="text" id="copa" value="{$team->copa}" class="number"/></td>
		 <td><label for="copa"></label></td>
		 </tr>
		 <tr><td>cucharas:</td><td><input type="text" id="cuchara" value="{$team->cuchara}" class="number"/></td>
		 <td><label for="cuchara"></label></td>
		 </tr>
		</table>
	</fieldset>
	</form>
	<div id="leyendaconfigure" class="leyenda">* campo opcional, el resto son obligatorios</div>
	</div>
eof;
return $html;
}

function get_summary_configure_team(){
	$html=<<<eof
	<fieldset class="summary_team">
	<legend id="summary_name"></legend>
	<ul>
	<li id="summary_manager1" title="primer entrenador"></li>
	<li id="summary_manager2" title="segundo entrenador"></li>
	<li id="summary_estadio" title="estadio"></li>
	<li id="summary_campeon" title="campeonatos"></li>
	<li id="summary_copa" title="copas"></li>
	<li id="summary_cuchara" title="cucharas"></li>
	</ul>
	</fieldset>
	<div id="div_btn_comeback">
	<input type="button" id="btn_comeback" class="button" value='volver'/>
	</div>
eof;
return $html;	
}

function set_data_configure($idinput,$value){
	$output="ok";
	$oBBDD=BBDD::get_instancia();
	$myid=$_SESSION["team"]["idequipo"];
	$sql="UPDATE `".get_pref()."_equipos` SET `".$idinput."`='".$value."' WHERE `id`=".$_SESSION["team"]["idequipo"];
	$oBBDD->set_resultados($sql);
	if ($idinput=="equipacion1"){
		if (@copy("../".ALLSHIRTS."/".$value, "../".SHIRTS."/".$value)){
			if (!@rename("../".ALLSHIRTS."/".$value, "../".ALLSHIRTS."/x".$value))
				$output="ko";
		}	
		else{
				$output="ko";
		}		
	}
	return $output;
}

function get_shirts_list(){
	$handle = opendir("../".ALLSHIRTS);
	$html="<div id='shirtscatalog' class='allshirts'>";
	$html .="<div class='rotulo'>elige tu camiseta</div>";
	$html .="<table>";
	$cont=0;
	while ($file = readdir($handle)){
		if (!is_dir($file)){
			if ( ($file != ".") && ($file != "..") && ($file[0] != "x") ){
				$idfile=strtok($file,".png");
				if ($cont==0)
					$html .="<tr>"; 
				$html .="<td><div id='".$idfile."' class='sampleshirt'><img src='".ALLSHIRTS."/".$file."' /></div></td>";
				$cont++;
				if ($cont==4){
					$html .="</tr>";
					$cont=0;
				}
			}
		}	
	}
	if ($cont!=4)
		$html .="</tr>";
	$html .="</table></div><div id='99' class='joder'></div>";
	$html .="<div id='div_btn_send_shirt'>";
	$html .="<input type='button' id='btn_send_shirt' class='button' value='guardar'/>";
	$html .="</div>";
	$html .="<div id='div_btn_cancel_shirt'>";
	$html .="<input type='button' id='btn_cancel_shirt' class='button' value='cancelar'/>";
	$html .="</div>";
	
	return $html;
	
}

?>


