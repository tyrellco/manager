<?php
/*
 *      rss.inc.php
 *      
 *      Copyright 2010 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
 
 
 function get_demarcacion($iddemarc){
	switch ($iddemarc){
		case 1: $demarc="POR";
						break;
		case 2: $demarc="DEF";
						break;
		case 3: $demarc="MED";
						break;
		case 4: $demarc="DEL";
						break;												
	}
	return $demarc;
}


function get_xml_clasifications_rss($tipo){
require_once ("classes/BBDD.class.php");
$oBBDD=BBDD::get_instancia();
$sql="SELECT `user`,`fecha` FROM `0_logupdates` ORDER BY `id` DESC  LIMIT 1";
$obj_clasificacion=$oBBDD->get_resource($sql);
$pubdate="sin actualizar";
$user="pendiente";

if (mysqli_num_rows($obj_clasificacion)>0){
	$data=mysqli_fetch_object($obj_clasificacion);
	$pubdate=$data->fecha;
	$user=$data->user;
}
	switch($tipo){
		//general
		case 1:$sql="SELECT e.id idequipo, e.nombre nombre, c.general puntos,c.regularidad FROM `0_clasificacion` c INNER JOIN ";
					 $sql .="`0_equipos` e ON (e.id=c.idequipo) ORDER by c.general DESC, c.regularidad DESC";
					 $xmlrss=<<<eof
						<item>
						<title>Clasificación General</title>
						<link>http://manager.antiliga.org/rss.php?feed=clasif</link>
						<pubDate>{$pubdate}</pubDate>
						<description><![CDATA[<ol>
eof;
					 break;
		//vuelta		 
		case 2:$sql="SELECT `vuelta` FROM `0_calendario` WHERE `numjornada`=8";
					 $obj_clasificacion=$oBBDD->get_resource($sql);
					 $clasificacion=mysqli_fetch_object($obj_clasificacion);
					 $vuelta="vuelta".$clasificacion->vuelta;
					 $sql="SELECT e.id idequipo, e.nombre nombre, c.".$vuelta." puntos FROM `0_clasificacion` c INNER JOIN ";
					 $sql .="`0_equipos` e ON (e.id=c.idequipo) ORDER by c.".$vuelta." DESC";
					 $xmlrss=<<<eof
						<item>
						<title>Así va la vuelta</title>
						<link>http://manager.antiliga.org/rss.php?feed=clasif</link>
						<pubDate>{$pubdate}</pubDate>
						<description><![CDATA[<ol>
eof;
					 break;
		//regularidad			 
		case 3:$sql="SELECT e.id idequipo, e.nombre nombre, c.regularidad puntos FROM `0_clasificacion` c INNER JOIN ";
					 $sql .="`0_equipos` e ON (e.id=c.idequipo) ORDER by c.regularidad DESC";
						$xmlrss=<<<eof
						<item>
						<title>Las verdades del barquero</title>
						<link>http://manager.antiliga.org/rss.php?feed=clasif</link>
						<pubDate>{$pubdate}</pubDate>
						<description><![CDATA[<ol>
eof;
					 break;			 			 
	}
	$obj_clasificacion=$oBBDD->get_resource($sql);
	while ( $clasificacion=mysqli_fetch_object($obj_clasificacion) ){
		$xmlrss .="<li>".$clasificacion->nombre." - ".$clasificacion->puntos." pts</li>";
	}
	$xmlrss .=<<<eof
	</ol>
	<div class='footer_rss'>{$pubdate} ({$user})</div>]]>
	</description>
	</item>
eof;
	return $xmlrss;

}

function get_xml_all_clasifications_rss(){
require_once ("classes/BBDD.class.php");
$oBBDD=BBDD::get_instancia();
$sql="SELECT `user`,`fecha` FROM `0_logupdates`";
$obj_clasificacion=$oBBDD->get_resource($sql);
$data=mysqli_fetch_object($obj_clasificacion);
$pubdate=$data->fecha;
$user=$data->user;
	$sql="SELECT e.id idequipo, e.nombre nombre, c.general puntos,c.regularidad FROM `0_clasificacion` c INNER JOIN ";
	$sql .="`0_equipos` e ON (e.id=c.idequipo) ORDER by c.general DESC, c.regularidad DESC";
	$xmlrss=<<<eof
	<item>
	<title>Clasificación General</title>
	<link>http://manager.antiliga.org/rss.php?feed=clasif</link>
	<pubDate>{$pubdate}</pubDate>
	<description><![CDATA[<ol>
eof;
$obj_clasificacion=$oBBDD->get_resource($sql);
	while ( $clasificacion=mysqli_fetch_object($obj_clasificacion) ){
		$xmlrss .="<li>".$clasificacion->nombre." - ".$clasificacion->puntos." pts</li>";
	}
	$xmlrss .=<<<eof
	</ol>
	<div class='footer_rss'>{$pubdate}, {$user}</div>]]>
	</description>
	</item>
eof;
$sql="SELECT `vuelta` FROM `0_calendario` WHERE `numjornada`=8";
$obj_clasificacion=$oBBDD->get_resource($sql);
$clasificacion=mysqli_fetch_object($obj_clasificacion);
$vuelta="vuelta".$clasificacion->vuelta;
$sql="SELECT e.id idequipo, e.nombre nombre, c.".$vuelta." puntos FROM `0_clasificacion` c INNER JOIN ";
$sql .="`0_equipos` e ON (e.id=c.idequipo) ORDER by c.".$vuelta." DESC";
$xmlrss .=<<<eof
				<item>
				<title>Así va la vuelta</title>
				<link>http://manager.antiliga.org/rss.php?feed=clasif</link>
				<pubDate>{$pubdate}</pubDate>
				<description><![CDATA[<ol>
eof;
$obj_clasificacion=$oBBDD->get_resource($sql);
	while ( $clasificacion=mysqli_fetch_object($obj_clasificacion) ){
		$xmlrss .="<li>".$clasificacion->nombre." - ".$clasificacion->puntos." pts</li>";
}
	$xmlrss .=<<<eof
	</ol>
	<div class='footer_rss'>{$pubdate}, {$user}</div>]]>
	</description>
	</item>
eof;
$sql="SELECT e.id idequipo, e.nombre nombre, c.regularidad puntos FROM `0_clasificacion` c INNER JOIN ";
$sql .="`0_equipos` e ON (e.id=c.idequipo) ORDER by c.regularidad DESC";
$xmlrss .=<<<eof
				<item>
				<title>La verdades del barquero</title>
				<link>http://manager.antiliga.org/rss.php?feed=clasif</link>
				<pubDate>{$pubdate}</pubDate>
				<description><![CDATA[<ol>
eof;
$obj_clasificacion=$oBBDD->get_resource($sql);
	while ( $clasificacion=mysqli_fetch_object($obj_clasificacion) ){
		$xmlrss .="<li>".$clasificacion->nombre." - ".$clasificacion->puntos." pts</li>";
}
	$xmlrss .=<<<eof
	</ol>
	<div class='footer_rss'>{$pubdate}, {$user}</div>]]>
	</description>
	</item>
eof;
	return $xmlrss;

}

function get_xml_last_results(){
	require_once ("classes/BBDD.class.php");
	$oBBDD=BBDD::get_instancia();
	
	$now=time();
	$sql="SELECT `numjornada` currentjornada FROM `0_calendario` WHERE `fechaunix`<".$now." AND `mostrar`<>0 ORDER BY `numjornada` DESC LIMIT 1";
	$obj_calendar=$oBBDD->get_resource($sql);
	$calendar=mysqli_fetch_object($obj_calendar); //última jornada jugada
	$idjornada=$calendar->currentjornada;
	
	$sql="SELECT `numjornada`,`fecha` FROM `0_calendario` WHERE `numjornada`=".$idjornada;
	$obj_results=$oBBDD->get_resource($sql);
	$results=mysqli_fetch_object($obj_results);
	$idjornada=$results->numjornada;
	$fecha=$results->fecha;

	$sql="SELECT SUM(a.puntos) puntos,e.nombre nombre,e.id idequipo FROM `0_alineaciones` a INNER JOIN `0_equipos` e ";
	$sql .="ON (a.idequipo=e.id) WHERE a.jornada=".$idjornada." GROUP BY (e.nombre)";
	$obj_matches=$oBBDD->get_resource($sql);
	if (mysqli_num_rows($obj_matches) == 14){
			while ( $matches=mysqli_fetch_object($obj_matches) )
				$vresults[$matches->idequipo]=$matches->nombre."#".$matches->puntos;
				
	$sql="SELECT `idequipo1`,`idequipo2` FROM `0_enfrentamientos` where `numjornada`=".$idjornada;
	$title="Resultados Jornada ".$idjornada."ª";
	$xmlresult=<<<eof
<item>
<title>{$title}</title>
<link>http://manager.antiliga.org/rss.php?feed=last</link>
<description><![CDATA[<table>
eof;
	$xmlresult .="<table>";
	$obj_results=$oBBDD->get_resource($sql);
	$class="";
	while ( $results=mysqli_fetch_object($obj_results) ){
		if ($class=="")
			$class="class='painted'";
		else
			$class="";	
	list($equipo1,$puntos1)=explode("#",$vresults[$results->idequipo1]);
	list($equipo2,$puntos2)=explode("#",$vresults[$results->idequipo2]);
	$xmlresult .="<tr ".$class."><td class='name'>".$equipo1."</td><td class='pto'>".$puntos1."</td></tr>";
	$xmlresult .="<tr ".$class."><td class='name'>".$equipo2."</td><td class='pto'>".$puntos2."</td></tr>";
}
$xmlresult .=<<<eof
</table>]]>
</description>
</item>
eof;
}
return $xmlresult;
	
}


function get_eleven_rss(){
	require_once ("classes/BBDD.class.php");
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT DISTINCT e.nombre equipo,a.idequipo idequipo,a.jornada jornada FROM `0_equipos` e INNER ";
	$sql .="JOIN `0_alineaciones` a ON a.idequipo=e.id ORDER BY a.id DESC";
	$obj_eleven=$oBBDD->get_resource($sql);
	$xmlrss="";
	if (mysqli_num_rows($obj_eleven) > 0){
			while ( $eleven=mysqli_fetch_object($obj_eleven) ){
				$sql="SELECT j.nombre nombre,j.demarcacion dem, a.fecha fecha FROM `0_jugadores` j INNER JOIN `0_alineaciones` a ";
				$sql .="ON a.idjugador=j.id WHERE a.idequipo=".$eleven->idequipo." AND a.jornada=".$eleven->jornada;
				$sql .=" ORDER BY dem";
				$obj_player=$oBBDD->get_resource($sql);
				//saco el primer jugador para pillar la fecha de puesta y hacerla coincidir con la fecha de publicación
				$player=mysqli_fetch_object($obj_player);
				$demarcacion=get_demarcacion($player->dem);
				$pubdate=$player->fecha;
				$prexml="<li>".$player->nombre." - ".$demarcacion."</li>";
				$xmlrss .=<<<eof
				<item>
				<title>Jornada {$eleven->jornada}, equipo de {$eleven->equipo}</title>
				<link>http://manager.antiliga.org/rss.php?feed=alin</link>
				<pubDate>{$pubdate}</pubDate>
				<description><![CDATA[<ol>{$prexml}
eof;
				while ( $player=mysqli_fetch_object($obj_player) ){ //sigo con el resto de jugadores
					$demarcacion=get_demarcacion($player->dem);
					$xmlrss .="<li>".$player->nombre." - ".$demarcacion."</li>";
				}
				$xmlrss .=<<<eof
				</ol>
				<div>Fecha y hora de presentación: {$pubdate}</div>]]>
				</description>
				</item>
eof;
			}
	}
	return $xmlrss;		
}

function get_all_eleven(){
	$xmleleven=get_eleven_rss();
	$xml=<<<eof
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
<channel>
<title>antiliga-manager</title>
<link>http://manager.antiliga.org</link>
<description>manager RSS</description>
<language>es-es</language>
<webMaster>mariorubiales(arroba)gmail.com</webMaster>
<generator>manager-antiliga rss</generator>
<copyright>antiliga.org - 2010 -</copyright>
{$xmleleven}
</channel>
</rss>
eof;
	echo $xml;
}


function get_clasifications_rss($tipo){
	$xmlclasification=get_xml_clasifications_rss($tipo);
	$xml=<<<eof
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
<channel>
<title>antiliga-manager</title>
<link>http://manager.antiliga.org</link>
<description>manager RSS</description>
<language>es-es</language>
<webMaster>mariorubiales(arroba)gmail.com</webMaster>
<generator>manager-antiliga rss</generator>
<copyright>antiliga.org - 2010 -</copyright>
{$xmlclasification}
</channel>
</rss>
eof;
	echo $xml;
}

function get_all_clasifications_rss(){
$xmlclasification=get_xml_all_clasifications_rss();
$xml=<<<eof
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
<channel>
<title>antiliga-manager</title>
<link>http://manager.antiliga.org</link>
<description>manager RSS</description>
<language>es-es</language>
<webMaster>mariorubiales(arroba)gmail.com</webMaster>
<generator>manager-antiliga rss</generator>
<copyright>antiliga.org - 2010 -</copyright>
{$xmlclasification}
</channel>
</rss>
eof;
	echo $xml;
}
 
function get_last_results_rss(){
$xmlresults=get_xml_last_results();
$xml=<<<eof
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
<channel>
<title>antiliga-manager</title>
<link>http://manager.antiliga.org</link>
<description>manager RSS</description>
<language>es-es</language>
<webMaster>mariorubiales(arroba)gmail.com</webMaster>
<generator>manager-antiliga rss</generator>
<copyright>antiliga.org - 2010 -</copyright>
{$xmlresults}
</channel>
</rss>
eof;
	echo $xml;
}
 
?>
