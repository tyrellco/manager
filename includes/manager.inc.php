<?php
/*
 *      manager.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");

function get_lfp_matches(){
	$oBBDD=BBDD::get_instancia();
	$jornada=$_SESSION["team"]["jornadalfp"];
	$sql="SELECT `local`,`visitante` FROM `".get_pref()."_calendariolfp` WHERE `jornada`=".$jornada;
	$obj_lfp=$oBBDD->get_resource($sql);
	$htmlmatches="<table>";
	while ($lfp=mysqli_fetch_object($obj_lfp)){
		$htmlmatches .="<tr><td>".$lfp->local."</td><td>".$lfp->visitante."</td></tr>";
	}
	$htmlmatches .="</table>";
	$html=<<<eof
	<fieldset>
		 	<legend>jornada {$jornada} LFP</legend>
			<div id="datamatcheslfp">{$htmlmatches}</div>
	</fieldset>
eof;

return $html;
}


function next_enemy(){
	$oBBDD=BBDD::get_instancia();
	$myid=$_SESSION["team"]["idequipo"];
	$sql="SELECT `idequipo1`, `idequipo2` FROM `".get_pref()."_enfrentamientos` WHERE ";
	$sql .="`numjornada`=".$_SESSION["team"]["numjornada"]." AND (`idequipo1`=".$myid." OR `idequipo2`=".$myid.");";

	$obj_data=$oBBDD->get_resource($sql);
	$data=mysqli_fetch_object($obj_data);
	$idenemy=$data->idequipo1;
	if ($idenemy==$myid)
		$idenemy=$data->idequipo2;
	$_SESSION["team"]["idenemy"]=$idenemy;
	$sql="SELECT eq.id id , eq.nombre nombre,eq.equipacion1 equipacion, eq.campeon campeon FROM `".get_pref()."_equipos` eq INNER JOIN ";
	$sql .="`".get_pref()."_clasificacion` c ON eq.id=c.idequipo WHERE c.idequipo=".$idenemy;
	$obj_data=$oBBDD->get_resource($sql);
	$data=mysqli_fetch_object($obj_data);
	$stars="";
	if ($data->campeon >0){
		for($i=0;$i<$data->campeon;$i++)
			$stars .="&#9733;";
	}
	$_SESSION["team"]["enemyname"]=$data->nombre;
	$html=<<<eof
	<fieldset>
		 	<legend>próximo rival</legend>
			<div id="shirtenemy"><img src="shirts/{$data->equipacion}" /></div>
			<div id="{$data->id}" class="nameteam">{$data->nombre}<span class='campeon'>{$stars}</span></div>
	</fieldset>
eof;

return $html;
}


function next_match(){
	$oBBDD=BBDD::get_instancia();
	$numjornada=$_SESSION["team"]["numjornada"];
	$sql="SELECT `numjornada`,`jornadalfp`, `fecha`, `vuelta`, `jornadant` FROM `".get_pref()."_calendario`";
	$sql .=" WHERE `numjornada`=".$numjornada.";";
	$obj_data=$oBBDD->get_resource($sql);
	$data=mysqli_fetch_object($obj_data);
	$html=<<<eof
	<fieldset>
		 <legend>pr&oacute;ximo partido</legend>
		<ul>
			<li>fecha: <span>{$data->fecha}</span></li>
			<li>jornada LFP: <span>{$data->jornadalfp}</span></li>
			<li>jornada antiliga: <span>{$data->jornadant}</span></li>
			<li>vuelta: <span>{$data->vuelta}&ordf;</span></li>
		<ul>
	</fieldset>
eof;

return $html;
}


function init_manager(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT COUNT(*) hay FROM `".get_pref()."_alineaciones` WHERE `jornada`=".$_SESSION["team"]["numjornada"];
	$sql .=" AND `idequipo`=".$_SESSION["team"]["idequipo"].";";
	$obj_alin=$oBBDD->get_resource($sql);
	$alin=mysqli_fetch_object($obj_alin);
	
	if ($alin->hay > 0) //si ya se ha puesto una alineación para esta jornada la mostramos
		return get_list_eleven($_SESSION["team"]["numjornada"]);
	else
		return "";
		
}
/* Obtiene la plantilla completa de un equipo*/
function get_team($id){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id ORDER BY `demarcacion`";
	$obj_team=$oBBDD->get_resource($sql);
	$stars=get_stars();

	$html="<div id='headteam'><div id='tshirtteam'>";
	$html .="<img src='".SHIRTS."/".$_SESSION["team"]["equipacion"]."'/></div>";
	$html .="<div id='nameteam'>".$_SESSION["team"]["nombre"]."<span class='campeon'>$stars</span></div></div>";
	$html .="<table>";
	$cont=1;
	
	while ($player=mysqli_fetch_object($obj_team)){
		$demtxt=get_demarcacion($player->demarcacion);
		$html .="<tr><td class='dorsal' id='dorsal_".$player->id."'>".$cont."</td>";
		if ($demtxt=="por")
			$html .="<td class='td".get_demarcacion($player->demarcacion)."' style=\"background-image:url('".SHIRTS."/gkshirts/".$_SESSION["team"]["equipaciongk"]."');\">";
		else
			$html .="<td class='td".get_demarcacion($player->demarcacion)."' style=\"background-image:url('".SHIRTS."/".$_SESSION["team"]["equipacion"]."');\">";
		$html .="<div id='".$player->id."' ";
		$html .="class='".get_demarcacion($player->demarcacion)."'>".$player->nombre."</div></td></tr>\n";
		$cont++;
	}
	
	$html .="</table>\n";

	return $html;
}

/* Obtiene la plantilla completa de un equipo en formato movil (jquery mobile)*/
function get_team_mobi($id){
	$oBBDD=BBDD::get_instancia();

	$sql="SELECT idjugador FROM `".get_pref()."_alineaciones` WHERE `jornada`=".$_SESSION["team"]["numjornada"];
	$sql .=" AND `idequipo`=".$id.";";
	$obj_alin=$oBBDD->get_resource($sql);
	$alin_array = array();
	while ($alin=mysqli_fetch_array($obj_alin)){
		$alin_array[$alin[0]] = true;
	}

	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=1 ORDER BY `nombre`";
	$obj_por=$oBBDD->get_resource($sql);
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=2 ORDER BY `nombre`";
	$obj_def=$oBBDD->get_resource($sql);
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=3 ORDER BY `nombre`";
	$obj_med=$oBBDD->get_resource($sql);
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=4 ORDER BY `nombre`";
	$obj_del=$oBBDD->get_resource($sql);
	
	$html=<<<eof
	<form id='form_team_mobi'>
		<fieldset data-type="vertical" id="group_gk">
			<legend><h2>porteros</h2></legend>
eof;

	$cont=1;
	while ($player=mysqli_fetch_object($obj_por)){
		$checked = isset($alin_array[$player->id]) ? "checked" : "";
		$player_name=$player->nombre;
		$html .=<<<eof
		<input name="{$player->id}" id="{$player->id}" type="checkbox" class='custom por' $checked>
		<label for="{$player->id}">{$player_name}</label>
eof;
		$cont++;
	}
	$html .="</fieldset>";
	
	$cont=1;
	$html .=<<<eof
	<form id="form_team_mobi">
	<fieldset data-type="vertical" id="group_def">
		<legend><h2>defensas</h2></legend>
eof;
	while ($player=mysqli_fetch_object($obj_def)){
		$checked = isset($alin_array[$player->id]) ? "checked" : "";
		$player_name=$player->nombre;
		$html .=<<<eof
		<input name="{$player->id}" id="{$player->id}" type="checkbox" class='def' $checked>
		<label for="{$player->id}">{$player_name}</label>
eof;
		$cont++;
	}
	$html .="</fieldset>";
	
	$cont=1;
	$html .=<<<eof
	<fieldset data-type="vertical" id="group_med">
		<legend><h2>medios</h2></legend>
eof;
	while ($player=mysqli_fetch_object($obj_med)){
		$checked = isset($alin_array[$player->id]) ? "checked" : "";
		$player_name=$player->nombre;
		$html .=<<<eof
		<input name="{$player->id}" id="{$player->id}" type="checkbox" class='med' $checked>
		<label for="{$player->id}">{$player_name}</label>
eof;
		$cont++;
	}
	$html .="</fieldset>";
	
	$cont=1;
	$html .=<<<eof
	<fieldset data-type="vertical" id="group_del">
		<legend><h2>delanteros</h2></legend>
eof;
	while ($player=mysqli_fetch_object($obj_del)){
		$checked = isset($alin_array[$player->id]) ? "checked" : "";
		$player_name=$player->nombre;
		$html .=<<<eof
		<input name="{$player->id}" id="{$player->id}" type="checkbox" class='del' $checked>
		<label for="{$player->id}">{$player_name}</label>
eof;
		$cont++;
	}
	$html .=<<<eof
	</fieldset>
	<div class="twobuttons" data-type="horizontal">
		<table>
			<tr>
				<td><a href="#" class="ui-btn ui-btn-inline ui-btn-icon-left ui-icon-delete ui-corner-all ui-shadow" id="btn_reset_form_mobi">cancelar</a></td>
				<td><a href="#" class="ui-btn ui-btn-inline ui-btn-icon-left ui-icon-check ui-corner-all ui-shadow" id="btn_send_team_mobi">enviar equipo</a></td>
			</tr>
		</table>
	</div>
eof;

	return $html;
}

/* Función deprecada por la anterior*/
function get_team_mobi_deprecated($id){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=1 ORDER BY `nombre`";
	$obj_por=$oBBDD->get_resource($sql);
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=2 ORDER BY `nombre`";
	$obj_def=$oBBDD->get_resource($sql);
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=3 ORDER BY `nombre`";
	$obj_med=$oBBDD->get_resource($sql);
	$sql="SELECT `id`,`nombre`,`demarcacion` FROM `".get_pref()."_jugadores` WHERE `idprop`=$id AND `demarcacion`=4 ORDER BY `nombre`";
	$obj_del=$oBBDD->get_resource($sql);
	
	$html="<form id='form_team_mobi'>";
	$html .="<div id='por_mobi'>";
	$html .="<table>";
	$html .="<tr><th colspan=3>PORTEROS</th></tr>";
	$cont=1;
	while ($player=mysqli_fetch_object($obj_por)){
		$html .="<tr><td class='dorsal'>".$cont."</td>";
		$html .="<td class='nameplayer'>".$player->nombre."</td>\n";
		$html .="<td><input type='checkbox' id='".$player->id."' class='por'></td></tr>\n";
		$cont++;
	}
	$html .="</table></div>\n";
	
	$html .="<div id='def_mobi'>";
	$html .="<table>";
	$html .="<tr><th colspan=3>DEFENSAS</th></tr>";
	$cont=1;
	while ($player=mysqli_fetch_object($obj_def)){
		$html .="<tr><td class='dorsal'>".$cont."</td>";
		$html .="<td class='nameplayer'>".$player->nombre."</td>\n";
		$html .="<td><input type='checkbox' id='".$player->id."' class='def'></td></tr>\n";
		$cont++;
	}
	$html .="</table></div>\n";
	
	$html .="<div id='med_mobi'>";
	$html .="<table>";
	$html .="<tr><th colspan=3>MEDIOS</th></tr>";
	$cont=1;
	while ($player=mysqli_fetch_object($obj_med)){
		$html .="<tr><td class='dorsal'>".$cont."</td>";
		$html .="<td class='nameplayer'>".$player->nombre."</td>\n";
		$html .="<td><input type='checkbox' id='".$player->id."' class='med'></td></tr>\n";
		$cont++;
	}
	$html .="</table></div>\n";
	
	$html .="<div id='del_mobi'>";
	$html .="<table>";
	$html .="<tr><th colspan=3>DELANTEROS</th></tr>\n";
	$cont=1;
	while ($player=mysqli_fetch_object($obj_del)){
		$html .="<tr><td class='dorsal'>".$cont."</td>\n";
		$html .="<td class='nameplayer'>".$player->nombre."</td>\n";
		$html .="<td><input type='checkbox' id='".$player->id."' class='del'/></td></tr>\n";
		$cont++;
	}
	$html .="</table></div>";
	$html .="<input type='button' class='btn_mobi' id='btn_reset_form_mobi' value='&lt;&lt; cancelar' /><input type='button' class='btn_mobi' id='btn_send_team_mobi' value='enviar equipo &gt;&gt;' />";
	
	return $html;
}

/* Elimina la ultima alineación presentada */
function remove_last_eleven(){
	
	if (get_pref()==0){ //No borramos ninguna alineación de años anteriores, sólo funciona para alineaciones del año en curso
		$oBBDD=BBDD::get_instancia();
		$sql="DELETE FROM `".get_pref()."_alineaciones` WHERE `jornada`=".$_SESSION["team"]["numjornada"];
		$sql .=" AND `idequipo`=".$_SESSION["team"]["idequipo"].";";
		$oBBDD->set_resultados($sql);
		/*$msgtwitter="Atención chicas, ".$_SESSION["userinfo"]["username"]." acaba de eliminar la última alineación de ".$_SESSION["nombre"];
		status_twitter($msgtwitter);*/
		$html="<div id='msg' class='info'>Tu once ha sido eliminado, vuelves a empezar en 3 seg...</div>";
	}
	else
		$html="<div id='msg' class='error'>No se pueden eliminar alineaciones del pasado ...</div>";
	return $html;
}

/* Crear una variable de sesion (vector) con el último once presentado */
function get_eleven($idjornada){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT j.nombre nombre, a.idjugador idjugador, a.idpos idpos, a.capitan capitan,a.puntos puntos ";
	$sql .="FROM `".get_pref()."_alineaciones` a ";
	$sql .="INNER JOIN `jugadores` j ON (j.id=a.idjugador) ";
	$sql .="WHERE (a.jornada=".$idjornada." AND a.idequipo=".$_SESSION["team"]["idequipo"].")";
	$obj_alin=$oBBDD->get_resource($sql);
	while ($alin=mysqli_fetch_object($obj_alin)){
		$grass[$alin->idpos]=$alin->nombre;
		if ($alin->capitan!=0)
			$idcapitan=$alin->capitan;
		$idtactic=substr($alin->idpos, 1,3);	
	}
	return get_grass_with_eleven($idtactic,$grass,$idcapitan);
}


/* Retorna en modo lista el once inicial de una jornada determinada */
function get_list_eleven($numjornada){
	$oBBDD=BBDD::get_instancia();
	
	$txtlog="";
	//Primero comprobamos si la tabla de log_alineaciones existe, ya que se implementó en el año 2012, para años anteriores no existe
	$checksql="SHOW TABLES LIKE '".get_pref()."_logalineaciones'";
	$obj_check=$oBBDD->get_resource($checksql);
	if (mysqli_num_rows($obj_check) > 0){
		$sql="SELECT * FROM `".get_pref()."_logalineaciones` WHERE `idequipo`=".$_SESSION["team"]["idequipo"]." AND `jornada`=".$_SESSION["team"]["numjornada"];
		$sql .=" ORDER BY `id` DESC LIMIT 1";
		$obj_log=$oBBDD->get_resource($sql);
		if (mysqli_num_rows($obj_log)>0){
			$log=mysqli_fetch_object($obj_log);
			$createdby=$log->user." (".$log->fecha.")";
			$txtlog="Equipo creado por ".$createdby;
		}
		else
			$txtlog="Equipo creado por: (no hay información en el log)";	
	}
	$html="<fieldset><legend>tus once chavales</legend>";
	$html .="<ul>";
	$sql="SELECT j.nombre nombre,j.demarcacion dem FROM `".get_pref()."_jugadores` j INNER JOIN `".get_pref()."_alineaciones` a ";
	$sql .="ON a.idjugador=j.id WHERE a.idequipo=".$_SESSION["team"]["idequipo"];
	$sql .=" AND a.jornada=".$numjornada." ORDER BY demarcacion";
	$obj_list=$oBBDD->get_resource($sql);
	while ($list=mysqli_fetch_object($obj_list)){
		$html .="<li>".$list->nombre."</li>";
	}		
	$html .="</ul>";
	$html .="<div id='log_eleven' class='leyenda'>".$txtlog."</div></fieldset>";	
	$html .="<div id='div_come_back' class='come_back_manager'>";
	$html .="<input type='button' id='btn_go_home' class='button' value='volver'/>";
	$html .="</div>";
	
	return $html;
}

function prepare_eleven_mobi($eleven_sausage){
	$eleven=array();
	
	$veleven_sausage=explode(";",$eleven_sausage);
	foreach ($veleven_sausage as $player_sausage){
		$vplayer_sausage=explode("#",$player_sausage);
		$veleven[$vplayer_sausage[0]]=$vplayer_sausage[1];	
	}
	$_SESSION["alineacion"]=$veleven;
	return set_initial_eleven();
}


/* Envia el once inicial a la BBDD */ 
function set_initial_eleven(){
	if ( (isset ($_SESSION["alineacion"])) && (isset ($_SESSION["team"])) ){
		$oBBDD=BBDD::get_instancia();
		
		if (! isset ($_SESSION["capitan"]))
			$idcapitan=0;
		else
			$idcapitan=$_SESSION["capitan"];
		// antes de nada nos cepillamos cualquier alineación anterior para esta jornada		
		$sql="DELETE FROM `".get_pref()."_alineaciones` WHERE `jornada`=".$_SESSION["team"]["numjornada"];
		$sql .=" AND `idequipo`=".$_SESSION["team"]["idequipo"].";";
		$oBBDD->set_resultados($sql);
		$momento=strftime("%a, %d %b %Y %H:%M:%S %Z");
		$msgtime=strftime("%H:%M:%S");//solo hora, min y segundos para acortar los mensajes en SMS y Twitter
		$current_week = $_SESSION['team']['numjornada'];

		$sql="INSERT INTO `".get_pref()."_alineaciones` (`jornada`, `idjugador`, `idequipo`, `fecha`, `idpos`, `jugado`) VALUES ";
		foreach($_SESSION["alineacion"] as $playerid=>$idpos){
			$sql .="($current_week,".$playerid.",".$_SESSION['team']['idequipo'].",'$momento', '$idpos', 1),";
		}

		$sql = rtrim($sql, ",");
		$oBBDD->set_resultados($sql);

		if ($_SESSION['team']['idequipo'] != 10){
			$sql = "SELECT count(*) players FROM `".get_pref()."_alineaciones` WHERE `idequipo` = 10 AND `jornada` = ".$_SESSION['team']['numjornada'];
			$obj_players=$oBBDD->get_resource($sql);
			$row=mysqli_fetch_object($obj_players);
			if ($row->players == 0 ){
				$current_week = $_SESSION['team']['numjornada'];
				$no_more_rabs_sql = "INSERT INTO `".get_pref()."_alineaciones` (`jornada`, `idjugador`, `idequipo`, `fecha`, `idpos`, `jugado`) VALUES ";
				$no_more_rabs_sql .= "($current_week,877,10,'$momento', '_433gk', 1),($current_week,1218,10,'$momento', '_433df1', 1),($current_week,302,10,'$momento', '_433df2', 1),($current_week,255,10,'$momento', '_433df3', 1),($current_week,1491,10,'$momento','_433df4', 1),($current_week,350,10,'$momento', '_433mid1', 1),($current_week,309,10,'$momento', '_433mid2', 1),($current_week,81,10,'$momento', '_433mid3', 1),($current_week,159,10,'$momento', '_433forw1', 1),($current_week,478,10,'$momento', '_433forw2', 1),($current_week,65,10,'$momento', '_433forw3', 1)";
				$oBBDD->set_resultados($no_more_rabs_sql);
			}
		}

		$vmsg=prepare_sms_and_twitter($_SESSION["alineacion"],$_SESSION["team"]["idequipo"]);
		
		/**********ENVIO DE SMS*******************/
		if ($vmsg["sms"]!="no"){
			$body="El equipo de ".$_SESSION["team"]["nombre"].": ".$vmsg["sms"]." (".$msgtime.")";
			$body=str_replace("#","",$body);// en los SMS eliminamos la marca de mitad de mensaje, no hace falta hacer un corte.
			send_sms ($_SESSION["team"]["idenemy"],$body,$_SESSION["team"]["idequipo"],$_SESSION["team"]["numjornada"]);
		}
		/*******************************************/
		
		/**********ENVIO DE TWITTER*******************/
		if ($vmsg["twitter"]!="no"){
			$body=$_SESSION["team"]["nombre"].": ".$vmsg["twitter"]." (".$msgtime.")";
			//send_twitter($_SESSION["team"]["idenemy"],$body,$_SESSION["team"]["idequipo"],$_SESSION["team"]["numjornada"]);
			//Enviamos el twitter directamente al timeline de la antiliga, no hace falta pasar por send_twitter
			status_twitter($body);
		}	
		/*******************************************/
		
		/********ACTUALIZAMOS EL LOG DE ALINEACIONES*******/
		$momento=strftime("%a, %d %b %Y %H:%M:%S %Z");
		$sql="INSERT INTO `".get_pref()."_logalineaciones` SET `jornada`=".$_SESSION["team"]["numjornada"].", ";
		$sql .="`user`='".$_SESSION["userinfo"]["username"]."', `fecha`='".$momento."', `idequipo`=".$_SESSION["team"]["idequipo"];
		$oBBDD->set_resultados($sql);
		
		/*******************************************/
					
		$html=get_list_eleven($_SESSION["team"]["numjornada"]);
	}
	else
	{
		$html="Arggg, ha ocurrido un error, sal de la aplicación, vuelve a entrar e inténtalo de nuevo";
	}
	unset($_SESSION["alineacion"]);
	unset($_SESSION["capitan"]);

	return $html;		
}
/* Presenta la lista de tácticas disponibles */
function get_list_tactics(){
	$html=<<<eof
	<fieldset>
		 <legend>elige tu táctica</legend>
		 <div id="alltactics">
				<div id="541" class="divtactic">5 - 4 - 1</div>
				<div id="532" class="divtactic">5 - 3 - 2</div>
				<div id="451" class="divtactic">4 - 5 - 1</div>
				<div id="442" class="divtactic">4 - 4 - 2</div>
				<div id="433" class="divtactic">4 - 3 - 3</div>
				<div id="343" class="divtactic">3 - 4 - 3</div>
				<div id="352" class="divtactic">3 - 5 - 2</div>
			</div>
		</fieldset>
		<div id="divformtactic">
			<form name="formtactic" id="formtactic" action="#" method="post">
				<input type="hidden" value="0" id="idtactic" />
				<input type="button" id="btn_get_grass" class="button" value="continuar"/>
			</form>
		</div>	
			<div id="div_btn_cancel_tactic">
				<input type="button" id="btn_cancel_tactic" class="button" value="cancelar"/>
			</div>
eof;
return $html;
}
/* Pone en el tapiz la táctica seleccionada */
function get_tactica($idtactic){
	switch ($idtactic){
		case 451:{	
				$html=<<<eof
				<div id="infotactic">4 - 5 - 1</div>
				<div id="_451gk" class="dropblpor"></div>
				 <div id="_451df1" class="dropbldef"></div>
				 <div id="_451df2" class="dropbldef"></div>
				 <div id="_451df3" class="dropbldef"></div>
				 <div id="_451df4" class="dropbldef"></div>
				 <div id="_451mid1" class="dropblmed"></div>
				 <div id="_451mid2" class="dropblmed"></div>
				 <div id="_451mid3" class="dropblmed"></div>
				 <div id="_451mid4" class="dropblmed"></div>
				 <div id="_451mid5" class="dropblmed"></div>
				 <div id="_451forw1" class="dropbldel"></div>
eof;
				break;
		}
		
			case 433:{	
			$html=<<<eof
			<div id="infotactic">4 - 3 - 3</div>
			<div id="_433gk" class="dropblpor"></div>
			 <div id="_433df1" class="dropbldef"></div>
			 <div id="_433df2" class="dropbldef"></div>
			 <div id="_433df3" class="dropbldef"></div>
			 <div id="_433df4" class="dropbldef"></div>
			 <div id="_433mid1" class="dropblmed"></div>
			 <div id="_433mid2" class="dropblmed"></div>
			 <div id="_433mid3" class="dropblmed"></div>
			 <div id="_433forw1" class="dropbldel"></div>
			 <div id="_433forw2" class="dropbldel"></div>
			 <div id="_433forw3" class="dropbldel"></div>
eof;
			break;
		}
		
			case 442:{	
			$html=<<<eof
			<div id="infotactic">4 - 4 - 2</div>
			<div id="_442gk" class="dropblpor"></div>
			 <div id="_442df1" class="dropbldef"></div>
			 <div id="_442df2" class="dropbldef"></div>
			 <div id="_442df3" class="dropbldef"></div>
			 <div id="_442df4" class="dropbldef"></div>
			 <div id="_442mid1" class="dropblmed"></div>
			 <div id="_442mid2" class="dropblmed"></div>
			 <div id="_442mid3" class="dropblmed"></div>
			 <div id="_442mid4" class="dropblmed"></div>
			 <div id="_442forw1" class="dropbldel"></div>
			 <div id="_442forw2" class="dropbldel"></div>
eof;
			break;
		}
		case 532:{	
				$html=<<<eof
				<div id="infotactic">5 - 3 - 2</div>
				<div id="_532gk" class="dropblpor"></div>
				 <div id="_532df1" class="dropbldef"></div>
				 <div id="_532df2" class="dropbldef"></div>
				 <div id="_532df3" class="dropbldef"></div>
				 <div id="_532df4" class="dropbldef"></div>
				 <div id="_532df5" class="dropbldef"></div>
				 <div id="_532mid1" class="dropblmed"></div>
				 <div id="_532mid2" class="dropblmed"></div>
				 <div id="_532mid3" class="dropblmed"></div>
				 <div id="_532forw1" class="dropbldel"></div>
				 <div id="_532forw2" class="dropbldel"></div>
eof;
				break;
		}
		case 541:{	
				$html=<<<eof
				<div id="infotactic">5 - 4 - 1</div>
				<div id="_541gk" class="dropblpor"></div>
				 <div id="_541df1" class="dropbldef"></div>
				 <div id="_541df2" class="dropbldef"></div>
				 <div id="_541df3" class="dropbldef"></div>
				 <div id="_541df4" class="dropbldef"></div>
				 <div id="_541df5" class="dropbldef"></div>
				 <div id="_541mid1" class="dropblmed"></div>
				 <div id="_541mid2" class="dropblmed"></div>
				 <div id="_541mid3" class="dropblmed"></div>
				 <div id="_541mid4" class="dropblmed"></div>
				 <div id="_541forw1" class="dropbldel"></div>
eof;
				break;
		}
		case 343:{
			$html=<<<eof
			<div id="infotactic">3 - 4 - 3</div>
			<div id="_343gk" class="dropblpor"></div>
			 <div id="_343df1" class="dropbldef"></div>
			 <div id="_343df2" class="dropbldef"></div>
			 <div id="_343df3" class="dropbldef"></div>
			 <div id="_343mid1" class="dropblmed"></div>
			 <div id="_343mid2" class="dropblmed"></div>
			 <div id="_343mid3" class="dropblmed"></div>
			 <div id="_343mid4" class="dropblmed"></div>
			 <div id="_343forw1" class="dropbldel"></div>
			 <div id="_343forw2" class="dropbldel"></div>
			 <div id="_343forw3" class="dropbldel"></div>
eof;
		break;
	}
		case 352:{	
				$html=<<<eof
				<div id="infotactic">3 - 5 - 2</div>
				<div id="_352gk" class="dropblpor"></div>
				 <div id="_352df1" class="dropbldef"></div>
				 <div id="_352df2" class="dropbldef"></div>
				 <div id="_352df3" class="dropbldef"></div>
				 <div id="_352mid1" class="dropblmed"></div>
				 <div id="_352mid2" class="dropblmed"></div>
				 <div id="_352mid3" class="dropblmed"></div>
				 <div id="_352mid4" class="dropblmed"></div>
				 <div id="_352mid5" class="dropblmed"></div>
				 <div id="_352forw1" class="dropbldel"></div>
				 <div id="_352forw2" class="dropbldel"></div>
eof;
				break;
		}			
	}
	$html .="<div id='infodrag' class='leyenda'>";
	$html .="<table><tr><td>* arrastra los jugadores a su demarcaci&oacute;n en el cesped</td></tr>";
	$html .="<tr><td>** doble clic sobre un jugador para eliminarlo</td></tr></table></div>";
	$html .="<div id='div_btn_send_data'>";
	$html .="<input type='button' id='btn_send_data' class='button' value='guardar'/>";
	$html .="</div>";
	$html .="<div id='div_btn_cancel_grass'>";
	$html .="<input type='button' id='btn_cancel_grass' class='button' value='cancelar'/>";
	$html .="</div>";
	
	return $html;
}

function prepare_sms_and_twitter($theeleven,$idequipo){
	$jornada=$_SESSION["team"]["numjornada"]; //jornada que se va a disputar
	$jornadaunix=$_SESSION["team"]["fechaunix"]; //fecha unix de la jornada que se va a jugar
	$momentounix=time();
	$datesec=172800;// 48 horas en segundos
	$intervalo=$jornadaunix-$momentounix;
	
	//Por defecto los mensajes no se mandan
	$vmsg["sms"]="no";
	$vmsg["twitter"]="no";
	
	// si quedan menos de 48 horas para el comienzo de la jornada (domingo 0:00), preparamos los mensajes para que se pueda enviar
	if ($intervalo < $datesec){
		$oBBDD=BBDD::get_instancia();
		$sql="SELECT COUNT(*) enviado FROM `".get_pref()."_smssend` WHERE `idequipo`=".$idequipo." AND `jornada`=".$jornada;
		$obj_smssend=$oBBDD->get_resource($sql);
		$hay=mysqli_fetch_object($obj_smssend);
		$msgbody="";
		$cont=0;
		foreach($theeleven as $playerid=>$idpos){
			$cont++;
			$sql="SELECT `nombre` FROM `".get_pref()."_jugadores` WHERE id=".$playerid;
			$obj_player=$oBBDD->get_resource($sql);
			$player=mysqli_fetch_object($obj_player);
			//$short_nameplayer=short_name($player->nombre);
			//$msgbody .=$player->nombre.",";
			$msgbody .=short_name($player->nombre).",";
			if ($cont==6) $msgbody .="#"; //ponemos una marca en la mitad, por si luego hay que cortar el mensaje para Twitter.
		}
		$msgbody=substr($msgbody,0,strlen($msgbody)-1); //quitamos la última coma
			
		// comprobamos que no hayamos enviado mas de dos sms, sólo dos sms por jornada y equipo que están muy caros
		if ($hay->enviado <= 2)
			$smsbody=$msgbody; //para los envios por SMS codificamos el mensaje en UTF-8 para no tener problemas con tildes ni eñes
		else
			$smsbody="no";
		
		// el mensaje de twitter se envía siempre, no cuestan dinero
		$twitterbody=$msgbody;
		
		//$vmsg["sms"]=$smsbody; // desactivamos el envío de SMS
		$vmsg["twitter"]=$twitterbody;
	} 
	
	return $vmsg;
}

/*
 * Función de envío de SMS para PIENSASOLUTIONS, actualmente está en desuso porque hemos optado
 * por otro proveedor que es más barato.
 * viernes,  3 de octubre de 2014, 01:06:35 CEST
 */

function send_sms_PIENSASOLUTIONS ($idenemy,$body,$myid,$jornada){
	require_once (dirname(__FILE__)."/../lib/SMSSend.inc");
	
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `movil` FROM `users` WHERE `idequipo`=".$idenemy;
	$obj_tfo=$oBBDD->get_resource($sql);
	$esperar=mysqli_num_rows($obj_tfo);
		while ($tfo=mysqli_fetch_object($obj_tfo)){
			$sms=new smsItem;
			$sms->setAccount("manager@antiliga.org");
			$sms->setPwd("manager09");
			$sms->setTo($tfo->movil);
			$sms->setText($body);
			$sms->setFrom("ANTILIGA"); //remitente
			//Envío del mensaje
			$result = $sms->Send();
			$credit = $sms->getCredit();
			if ($esperar > 1) // si hay más de un movil para este equipo, esperamos medio segundo, para evitar problemas
				usleep(500000);
	}		
	$sql="INSERT INTO `".get_pref()."_smssend` SET `idequipo`=".$myid.", `jornada`=".$jornada;
	$sql .=", `resultado`='".$result."',`credito`=".$credit;
	$oBBDD->set_resultados($sql);
}

/*
 * Función de envio de SMS para SMSPUBLI, actualmente esa la función en producción
 * viernes,  3 de octubre de 2014, 01:06:35 CEST
 * 
 */

function send_sms ($idenemy,$body,$myid,$jornada){
	require_once (dirname(__FILE__)."/../lib/SendSMS.php");
	
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `movil` FROM `users` WHERE `idequipo`=".$idenemy;
	$obj_tfo=$oBBDD->get_resource($sql);
	$esperar=mysqli_num_rows($obj_tfo);
	
	$sms = new SMS();
	while ($tfo=mysqli_fetch_object($obj_tfo)){
		$sms->setDA($tfo->movil) or die("ERROR al enviar el SMS (setDA): ".$errstr);
		$sms->setSA(SMSSOURCE) or die("ERROR al enviar el SMS (setSA): ".$errstr);
		$sms->setUR(USERREFERENCE) or die("ERROR al enviar el SMS (setUR): ".$errstr);
		$sms->setDR(DELIVERYRECEIPTS) or die("ERROR al enviar el SMS (setDR): ".$errstr);
		$sms->setMSG($body) or die("ERROR al enviar el SMS (setMSG): ".$errstr);
		//Envío del SMS
		$result=send_sms_object($sms,SMSUSERNAME,SMSPASSWD) or die("ERROR al enviar el SMS (send_sms_object): ".$errstr);
		
		if ($esperar > 1) // si hay más de un movil para este equipo, esperamos medio segundo, para evitar problemas
			usleep(500000);
	}		
	$sql="INSERT INTO `".get_pref()."_smssend` SET `idequipo`=".$myid.", `jornada`=".$jornada;
	$sql .=", `resultado`='".$result."'";
	$oBBDD->set_resultados($sql);
}


/*
 * Esta función está deprecada, no se usa, en su lugar usamos status_twitter ya que los mensajes se mandan directamente al
 * timeline de la antiliga, no van diriguidos a nadie.
 */

function send_twitter ($idenemy,$body,$myid,$jornada){
	require_once (dirname(__FILE__)."/../lib/SMSSend.inc");
	
	$oBBDD=BBDD::get_instancia();
	$pattern="/@([A-Za-z0-9_]+)/";
	$sql="SELECT `twitter` FROM `users` WHERE `idequipo`=".$idenemy;
	$obj_twt=$oBBDD->get_resource($sql);
	$cont=0;
	while ($twt=mysqli_fetch_object($obj_twt)){
		if (preg_match ($pattern ,$twt->twitter)){//si es una direccion de twitter válida continuamos
			$cont++;
			status_twitter($body,$twt->twitter);
			if ($cont > 1) //si hay más de un usuario para este equipo, esperamos medio segundo antes de enviar el otro twitter, evitando problemas
				usleep(500000);
		}
	}
}

function status_twitter($msg) { 
  require_once (dirname(__FILE__)."/../lib/twitteroauth/twitteroauth.php");
   
  function getConnectionWithAccessToken() { 
    $connection = new TwitterOAuth(_CONSUMER_KEY, _CONSUMER_SECRET,_OAUTH_TOKEN, _OAUTH_TOKEN_SECRET);
		return $connection; 
  }
	
	//mandamos el mensaje directamente al timeline de la antiliga
	$fullmsg=$msg;
	$connection = getConnectionWithAccessToken();
	
	if (strlen($fullmsg) > 140){//Si el mensaje tiene más de 140 caracteres lo partimos y enviamos dos twits
		$vfullmsg=explode(",#", $fullmsg);
		$fullmsg=$vfullmsg[0]."... (cont)";
		$twitter = $connection->post('statuses/update', array('status' => $fullmsg));
		usleep(1000000); //espero un segundo antes de enviar la segunda parte
		//$fullmsg=$nick." ... ".$vfullmsg[1];
		//mandamos el mensaje directamente al timeline de la antiliga
		$fullmsg=$vfullmsg[1];
		$twitter = $connection->post('statuses/update', array('status' => $fullmsg));
	}
	else{
		$fullmsg=str_replace("#","",$fullmsg);// si el mensaje no es mayor de 140 caract elimino la marca de mitad de mensaje 
  	$twitter = $connection->post('statuses/update', array('status' => $fullmsg));
	}	
} 

/*Función que acorta los nombres de jugadores que tienen espacios así se solucionan los problemas de espacio en los SMS*/
function short_name($name){
	$pos=strpos($name," ");
	if ($pos){
		$letter=substr($name,0,1);
		$surname=substr($name,$pos+1,strlen($name));
		return $letter.".".$surname;
	}
	else
		return $name;
}
?>
