<?php
/*
 *      global.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
/*
 * */

require_once(dirname(__FILE__)."/../classes/BBDD.class.php");
require_once(dirname(__FILE__)."/../classes/MoseLog.class.php");


function is_admin(){
	return $_SESSION["userinfo"]["rol"];
}

function get_info_user(){
	$last=$_SESSION["userinfo"]["last"];
	$html=<<<eof
	<ul>
			<li id="username" title="usuario">{$_SESSION["userinfo"]["username"]}</li>
			<li id="last" title="última vez">{$last}</li>
			<li id="help">ayuda</li>
			<li id="logout">desconectar</li>
		</ul>
eof;
return $html;
}

function get_datateam($id){
	unset($_SESSION["alineacion"]);
	unset($_SESSION["capitan"]);
	unset($_SESSION["team"]);
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_equipos` WHERE `id`=$id";
	$obj_team=$oBBDD->get_resource($sql);
	$team=mysqli_fetch_object($obj_team);
	$now=time();
  /*El domingo seguirá mostrando la jornada del domingo y no la próxima, por eso le quitamos un día (86400 seg),
  	a veces al final de la liga todos los partidos se juegan en Domingo y de esta forma evitamos que el manager
  	muestre el partido de la semana que viene.
  */
	$now -=86400;
	$sql="SELECT * FROM `".get_pref()."_calendario` WHERE `fechaunix` > ".$now. " LIMIT 1";

	$obj_calendar=$oBBDD->get_resource($sql);
	$calendar=mysqli_fetch_object($obj_calendar);
	
	$datateam["idequipo"]=$id;
	$datateam["equipacion"]=$team->equipacion1;
	$datateam["campeon"]=$team->campeon;
	
	//La equipacion del portero es aleatoria entre las 11 que hay reservadas.
	$gkshirt=rand(1000,1014).".png";
	$datateam["equipaciongk"]=$gkshirt;
	
	$datateam["nombre"]=$team->nombre;
	$datateam["manager1"]=$team->manager1;
	$datateam["manager2"]=$team->manager2;
	$datateam["estadio"]=$team->estadio;
	
	if (mysqli_num_rows($obj_calendar) > 0){
		$datateam["numjornada"]=$calendar->numjornada;
		$datateam["jornadalfp"]=$calendar->jornadalfp;
		$datateam["vuelta"]=$calendar->vuelta;
		$datateam["fechaunix"]=$calendar->fechaunix;
	}
	else{ // Se ha terminado el año, ponemos como jornada actual la última jornada.
		$datateam["numjornada"]=38;
		$datateam["vuelta"]=3;
		$datateam["fechaunix"]=1273960800;
	}
	
	$now=time();
	$sql="SELECT `numjornada` currentjornada FROM `".get_pref()."_calendario` WHERE `fechaunix`<".$now." AND `mostrar`<>0 ORDER BY `numjornada` DESC LIMIT 1";
	$obj_calendar=$oBBDD->get_resource($sql);
	$calendar=mysqli_fetch_object($obj_calendar); //última jornada jugada
	$datateam["currentjornada"]=($calendar->currentjornada==0) ? 1: $calendar->currentjornada;
	
	if (!isset($_SESSION["year"])){
		$_SESSION["year"]=0;
		$_SESSION["txtyear"]="temporada actual";
	}
	
	$_SESSION["team"]=$datateam;
}

function get_demarcacion($iddemarc){

	switch ($iddemarc){
		case 1: $demarc="por";
						break;
		case 2: $demarc="def";
						break;
		case 3: $demarc="med";
						break;
		case 4: $demarc="del";
						break;
		default:$demarc="por";
						break;										
	}
	return $demarc;
}

function get_list_lastyears(){
	$oBBDD=BBDD::get_instancia();
	if (isset($_SESSION["year"]))
		$sql="SELECT `year`,`txtyear` FROM `years` WHERE `year`<>".$_SESSION["year"]." ORDER BY `year`";
	else
		$sql="SELECT `year`,`txtyear` FROM `years` ORDER BY `year`";
		
	$obj_year=$oBBDD->get_resource($sql);	
	$html="<select id='listlastyears'>";
	if (isset($_SESSION["year"]))
		$html .="<option value=".$_SESSION["year"]." selected>".$_SESSION["txtyear"]."</option>";
	else{
		$data=mysqli_fetch_object($obj_year);
		$html .="<option value=".$data->year." selected>".$data->txtyear."</option>";
	}
			
	while ($data=mysqli_fetch_object($obj_year))
		$html .="<option value=".$data->year.">".$data->txtyear."</option>";
		
	$html .="</select>";
	
	return $html;
}

function get_pref(){
	if (isset($_SESSION["year"]))
		return $_SESSION["year"];
	else
		return PRE;	
}

function send_twitter_msg($msg) { 
  require_once (dirname(__FILE__)."/../lib/twitteroauth/twitteroauth.php");
   
  $connection = new TwitterOAuth(_CONSUMER_KEY, _CONSUMER_SECRET,_OAUTH_TOKEN, _OAUTH_TOKEN_SECRET);
	$connection->post('statuses/update', array('status' => $msg));
		
} 

function get_random_manager(){
	$vmanager=array ("Bilardo","Griguol","Luxemburgo","David Vidal","Zagalo","Gorosito","Lobanovsky","López Caro");
	$idmanager=rand(0,7);
	return $vmanager[$idmanager];
}

function get_fecha_actual (){
	return date("d\/m\/Y\ -\ H:i:s");
}

function get_stars(){
	$stars="";
	if ($_SESSION["team"]["campeon"] >0){
		for($i=0;$i<$_SESSION["team"]["campeon"];$i++)
			$stars .="&#9733;";
	}
	
	return $stars;
}

function about(){
	$version=VERSION;
	$html=<<<eof
	<span id="version">antiliga-manager {$version}</span>
eof;

return $html;
}
?>
 
