<?php
/*
 *      desktop.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");

function get_team_status(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT COUNT(*) conf FROM `".get_pref()."_equipos` WHERE (`nombre`='' OR `estadio`='' OR `equipacion1`='') AND `id`=".$_SESSION["userinfo"]["idusuario"];
	$obj_status=$oBBDD->get_resource($sql);
	$status=mysqli_fetch_object($obj_status);
	return $status->conf;
}

function get_last_time_update(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `user`,`fecha` FROM `0_logupdates` ORDER BY `id` DESC  LIMIT 1";
	$html="";
	$obj_last=$oBBDD->get_resource($sql);
	if (mysqli_num_rows($obj_last)>0){
		$last=mysqli_fetch_object($obj_last);
		$html="&uacute;ltima actualizaci&oacute;n: ".$last->user.", ".$last->fecha;
	}	
	return $html;
}

function get_clasification($tipo){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `vuelta` FROM `".get_pref()."_calendario` WHERE `numjornada`=".$_SESSION["team"]["currentjornada"];
	switch($tipo){
		//general
		case 1:$sql="SELECT e.id idequipo, e.nombre nombre, e.equipacion1 shirt, c.general puntos,c.regularidad FROM `".get_pref()."_clasificacion` c INNER JOIN ";
					 $sql .="`".get_pref()."_equipos` e ON (e.id=c.idequipo) ORDER by c.general DESC, c.regularidad DESC";
					 $txttipo="gral";
					 $html="<table id='cgral'>";
					 break;
		//vuelta		 
		case 2:$sql="SELECT `vuelta` FROM `".get_pref()."_calendario` WHERE `numjornada`=".$_SESSION["team"]["currentjornada"];
					 $obj_clasificacion=$oBBDD->get_resource($sql);
					 $clasificacion=mysqli_fetch_object($obj_clasificacion);
					 $vuelta="vuelta".$clasificacion->vuelta;
					 $sql="SELECT e.id idequipo, e.nombre nombre, e.equipacion1 shirt, c.".$vuelta." puntos FROM `".get_pref()."_clasificacion` c INNER JOIN ";
					 $sql .="`".get_pref()."_equipos` e ON (e.id=c.idequipo) ORDER by c.".$vuelta." DESC, c.regularidad DESC";
					 $html="<table id='cvta'>";
					 $txttipo="vta";
					 break;
		//regularidad			 
		case 3:$sql="SELECT e.id idequipo, e.nombre nombre, e.equipacion1 shirt, c.regularidad puntos FROM `".get_pref()."_clasificacion` c INNER JOIN ";
					 $sql .="`".get_pref()."_equipos` e ON (e.id=c.idequipo) ORDER by c.regularidad DESC, c.general DESC";
					 $html="<table id='creg'>";
					 $txttipo="reg";
					 break;			 			 
	}
	$obj_clasificacion=$oBBDD->get_resource($sql);
	$cont=1;
	while ( $clasificacion=mysqli_fetch_object($obj_clasificacion) ){
		$idtd=$txttipo."_".$cont;
		$html .="<tr><td id='".$idtd."' class='posclas'>".$cont."&ordm;</td>";
		$html .="<td class='nomclas' id=".$clasificacion->idequipo." style=\"background-image:url('".SHIRTS."/".$clasificacion->shirt."');\">".$clasificacion->nombre."</td>";
		$html .="<td class='ptsclas'>".$clasificacion->puntos."</td></tr>";
		$cont++;
	}
	$html .="</table>";
		
	return $html;
}

function get_summary_team(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_equipos` WHERE id=".$_SESSION["team"]["idequipo"];
	$obj_team=$oBBDD->get_resource($sql);
	$team=mysqli_fetch_object($obj_team);
	if ($team->manager2=="")
		$manager2=ucwords(get_random_manager());
	else
		$manager2=$team->manager2;
	
	$stars=get_stars();
	
	$html=<<<eof
	<fieldset id="summary_myteam" class="summary_team_desktop">
	<legend id="summary_name_desktop">{$team->nombre}<span class='campeon'>{$stars}</span></legend>
	<ul>
	<li id="summary_manager1" title="primer entrenador"><span>entrenador: </span>{$team->manager1}</li>
	<li id="summary_manager2" title="segundo entrenador"><span>2&ordm; entrenador: </span>{$manager2}</li>
	<li id="summary_estadio" title="estadio"><span>estadio: </span>{$team->estadio}</li>
	<li id="summary_campeon" title="campeonatos"><span>campeonatos: </span>{$team->campeon}</li>
	<li id="summary_copa" title="copas"><span>copas: </span>{$team->copa}</li>
	<li id="summary_cuchara" title="cucharas"><span>cucharitas: </span>{$team->cuchara}</li>
	</ul>
	</fieldset>
eof;
return $html;	
}

function get_lastest_results($idjornada){
	$oBBDD=BBDD::get_instancia();
	
	if ($idjornada==0)
		$idjornada=$_SESSION["team"]["currentjornada"];

	$sql="SELECT `numjornada`,`fecha` FROM `".get_pref()."_calendario` WHERE `numjornada`=".$idjornada;
	$obj_results=$oBBDD->get_resource($sql);
	$results=mysqli_fetch_object($obj_results);
	$idjornada=$results->numjornada;
	$fecha=$results->fecha;
	
	if ($_SESSION["team"]["currentjornada"]<=$idjornada){
		$nextjornada=$_SESSION["team"]["currentjornada"];
		$prevjornada=$_SESSION["team"]["currentjornada"]-1;
	}	
	else{
		$nextjornada=$idjornada+1;
		$prevjornada=$idjornada-1;
		if ($prevjornada<=0)
			$prevjornada=$_SESSION["team"]["currentjornada"];
	}
	$sql="SELECT SUM(a.puntos) puntos,e.nombre nombre,e.id idequipo,e.equipacion1 shirt FROM `".get_pref()."_alineaciones` a INNER JOIN `".get_pref()."_equipos` e ";
	$sql .="ON (a.idequipo=e.id) WHERE a.jornada=".$idjornada." GROUP BY (e.nombre)";

	$obj_matches=$oBBDD->get_resource($sql);
	if (mysqli_num_rows($obj_matches) == 14){
			while ( $matches=mysqli_fetch_object($obj_matches) )
				$vresults[$matches->idequipo]=$matches->nombre."#".$matches->puntos."#".$matches->shirt;
				
			$sql="SELECT `idequipo1`,`idequipo2` FROM `".get_pref()."_enfrentamientos` where `numjornada`=".$idjornada;
			$html="<fieldset><legend class='rotulo'>&uacute;ltimos resultados</legend>";
			$html .="<input type='hidden' id='next_id' value=".$nextjornada." />";
			$html .="<input type='hidden' id='prev_id' value=".$prevjornada." />";
			$html .="<table>";
			$html .="<tr class='head'><td class='prev' title='jornada anterior'></td>";
			$html .="<th>jornada ".$idjornada."&ordf;<span>(".$fecha.")</span></th>";
			$html .="<td class='next' title='siguiente jornada'></td></tr>";
			$html .="</table><table>";
			$obj_results=$oBBDD->get_resource($sql);
			$class="";
			while ( $results=mysqli_fetch_object($obj_results) ){
				if ($class=="")
					$class="class='painted'";
				else
					$class="";	
				list($equipo1,$puntos1,$shirt1)=explode("#",$vresults[$results->idequipo1]);
				list($equipo2,$puntos2,$shirt2)=explode("#",$vresults[$results->idequipo2]);
				$html .="<tr ".$class."><td class='name' style=\"background-image:url('".SHIRTS."/".$shirt1."');\">".$equipo1."</td><td class='pto'>".$puntos1."</td></tr>";
				$html .="<tr ".$class."><td class='name' style=\"background-image:url('".SHIRTS."/".$shirt2."');\">".$equipo2."</td><td class='pto'>".$puntos2."</td></tr>";
			}
			$html .="</fieldset></table>";
		}
		else{
			$html="<fieldset><legend class='rotulo'>&uacute;ltimos resultados</legend>";
			$html .="<input type='hidden' id='next_id' value=".$nextjornada." />";
			$html .="<input type='hidden' id='prev_id' value=".$prevjornada." />";
			$html .="<table>";
			$html .="<tr class='head'><td class='prev'></td>";
			$html .="<th>jornada ".$idjornada."&ordf;<span>(".$fecha.")</span></th>";
			$html .="<td class='next'></td></tr>";
			$html .="</table><p>A&uacute;n no hay resultados para la jornada ".$idjornada."</p></fieldset>";
		}	
	
	return $html;
	
}

function get_dataenemy($idenemy){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_equipos` WHERE id=".$idenemy;
	$obj_team=$oBBDD->get_resource($sql);
	$team=mysqli_fetch_object($obj_team);
	$equipacion=SHIRTS."/".$team->equipacion1;
	if ($team->manager2=="")
		$manager2=get_random_manager();
	else
		$manager2=$team->manager2;
			
	$html=<<<eof
	<div id="closepopup"></div>
	<fieldset id="summary_enemy" class="summary_team_desktop">
	<legend id="summary_enemy_desktop" style="background-image:url('{$equipacion}')">{$team->nombre}</legend>
	<ul>
	<li id="summary_manager1" title="primer entrenador"><span>entrenador: </span>{$team->manager1}</li>
	<li id="summary_manager2" title="segundo entrenador"><span>2&ordm; entrenador: </span>{$manager2}</li>
	<li id="summary_estadio" title="estadio"><span>estadio: </span>{$team->estadio}</li>
	<li id="summary_campeon" title="campeonatos"><span>campeonatos: </span>{$team->campeon}</li>
	<li id="summary_copa" title="copas"><span>copas: </span>{$team->copa}</li>
	<li id="summary_cuchara" title="cucharas"><span>cucharitas: </span>{$team->cuchara}</li>
	</ul>
	</fieldset>
eof;
	$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=".$idenemy." ORDER BY `demarcacion` ASC, `nombre` ASC";	
	$obj_team=$oBBDD->get_resource($sql);
	$cont=1;
	$shirt=SHIRTS."/".$_SESSION["team"]["equipacion"];
	$html .=<<<eof
	<table>
	<tr class="head">
		<td colspan="2">jugador</td>
		<td>lfp</td>
		<td>coste</td>
		<td>goles</td>
		<td>puntos</td>
	</tr>
eof;
while ($player=mysqli_fetch_object($obj_team)){
		$html .="<tr class='data".get_demarcacion($player->demarcacion)."'><td class='onlydata'>".$cont."</td>";
		$html .="<td>";
		$html .=$player->nombre."</td>\n";
		$html .="<td class='onlydata'>".$player->equipolfp."</td>\n";
		$html .="<td class='onlydata'>".$player->coste."</td>\n";
		$html .="<td class='onlydata'>".$player->goles."</td>\n";
		$html .="<td class='onlydata'>".$player->puntos."</td><tr/>\n";
		$cont++;
	}
	$html .="</table>\n";
return $html;	
}

function set_pref($year,$txtyear){
	$_SESSION["year"]=$year;
	$_SESSION["txtyear"]=$txtyear;
	return 0;
}

function logout(){
	unset($_SESSION["alineacion"]);
	unset($_SESSION["capitan"]);
	unset($_SESSION["team"]);
	unset($_SESSION["userinfo"]);
	unset($_SESSION["year"]);
	unset($_SESSION["txtyear"]);
	echo 0;
}

?>

