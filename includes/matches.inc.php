<?php
/*
 *      matches.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");

/*
 * Retorna el html que planta los jugadores en el cesped con la tactica
 * que se estableció en su momento. La variable prefix será la que decida
 * si el equipo es el propio del usuario o el del rival
 * */
function get_tactic_in_grass($idtactic,$prefix,$shirt,$grass,$goals,$red){

		switch ($idtactic){
		case 343:{
			$html=<<<eof
			<div id="{$prefix}_343gk" class="dropblpor"><img src='{$shirt}' border='0'/><p{$red["_343gk"]}>{$grass["_343gk"]}</p>{$goals["_343gk"]}</div>
			 <div id="{$prefix}_343df1" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_343df1"]}>{$grass["_343df1"]}</p>{$goals["_343df1"]}</div>
			 <div id="{$prefix}_343df2" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_343df2"]}>{$grass["_343df2"]}</p>{$goals["_343df2"]}</div>
			 <div id="{$prefix}_343df3" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_343df3"]}>{$grass["_343df3"]}</p>{$goals["_343df3"]}</div>
			 <div id="{$prefix}_343mid1" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_343mid1"]}>{$grass["_343mid1"]}</p>{$goals["_343mid1"]}</div>
			 <div id="{$prefix}_343mid2" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_343mid2"]}>{$grass["_343mid2"]}</p>{$goals["_343mid2"]}</div>
			 <div id="{$prefix}_343mid3" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_343mid3"]}>{$grass["_343mid3"]}</p>{$goals["_343mid3"]}</div>
			 <div id="{$prefix}_343mid4" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_343mid4"]}>{$grass["_343mid4"]}</p>{$goals["_343mid4"]}</div>
			 <div id="{$prefix}_343forw1" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_343forw1"]}>{$grass["_343forw1"]}</p>{$goals["_343forw1"]}</div>
			 <div id="{$prefix}_343forw2" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_343forw2"]}>{$grass["_343forw2"]}</p>{$goals["_343forw2"]}</div>
			 <div id="{$prefix}_343forw3" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_343forw3"]}>{$grass["_343forw3"]}</p>{$goals["_343forw3"]}</div>
eof;
		break;
	}
		case 541:{
				$html=<<<eof
				<div id="{$prefix}_541gk" class="dropblpor"><img src='{$shirt}' border='0'/><p{$red["_541gk"]}>{$grass["_541gk"]}</p>{$goals["_541gk"]}</div>
				 <div id="{$prefix}_541df1" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_541df1"]}>{$grass["_541df1"]}</p>{$goals["_541df1"]}</div>
				 <div id="{$prefix}_541df2" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_541df2"]}>{$grass["_541df2"]}</p>{$goals["_541df2"]}</div>
				 <div id="{$prefix}_541df3" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_541df3"]}>{$grass["_541df3"]}</p>{$goals["_541df3"]}</div>
				 <div id="{$prefix}_541df4" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_541df4"]}>{$grass["_541df4"]}</p>{$goals["_541df4"]}</div>
				 <div id="{$prefix}_541df5" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_541df5"]}>{$grass["_541df5"]}</p>{$goals["_541df5"]}</div>
				 <div id="{$prefix}_541mid1" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_541mid1"]}>{$grass["_541mid1"]}</p>{$goals["_541mid1"]}</div>
				 <div id="{$prefix}_541mid2" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_541mid2"]}>{$grass["_541mid2"]}</p>{$goals["_541mid2"]}</div>
				 <div id="{$prefix}_541mid3" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_541mid3"]}>{$grass["_541mid3"]}</p>{$goals["_541mid3"]}</div>
				 <div id="{$prefix}_541mid4" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_541mid4"]}>{$grass["_541mid4"]}</p>{$goals["_541mid4"]}</div>
				 <div id="{$prefix}_541forw1" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_541forw1"]}>{$grass["_541forw1"]}</p>{$goals["_541forw1"]}</div>
eof;
			break;
		}
	
			case 532:{
				$html=<<<eof
				 <div id="{$prefix}_532gk" class="dropblpor"><img src='{$shirt}' border='0'/><p{$red["_532gk"]}>{$grass["_532gk"]}</p>{$goals["_532gk"]}</div>
				 <div id="{$prefix}_532df1" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_532df1"]}>{$grass["_532df1"]}</p>{$goals["_532df1"]}</div>
				 <div id="{$prefix}_532df2" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_532df2"]}>{$grass["_532df2"]}</p>{$goals["_532df2"]}</div>
				 <div id="{$prefix}_532df3" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_532df3"]}>{$grass["_532df3"]}</p>{$goals["_532df3"]}</div>
				 <div id="{$prefix}_532df4" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_532df4"]}>{$grass["_532df4"]}</p>{$goals["_532df4"]}</div>
				 <div id="{$prefix}_532df5" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_532df5"]}>{$grass["_532df5"]}</p>{$goals["_532df5"]}</div>
				 <div id="{$prefix}_532mid1" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_532mid1"]}>{$grass["_532mid1"]}</p>{$goals["_532mid1"]}</div>
				 <div id="{$prefix}_532mid2" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_532mid2"]}>{$grass["_532mid2"]}</p>{$goals["_532mid2"]}</div>
				 <div id="{$prefix}_532mid3" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_532mid3"]}>{$grass["_532mid3"]}</p>{$goals["_532mid3"]}</div>
				 <div id="{$prefix}_532forw1" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_532forw1"]}>{$grass["_532forw1"]}</p>{$goals["_532forw1"]}</div>
				 <div id="{$prefix}_532forw2" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_532forw2"]}>{$grass["_532forw2"]}</p>{$goals["_532forw2"]}</div>
eof;
			break;
		}
	
			case 451:{
			$html=<<<eof
			<div id="{$prefix}_451gk" class="dropblpor"><img src='{$shirt}' border='0'/><p{$red["_451gk"]}>{$grass["_451gk"]}</p>{$goals["_451gk"]}</div>
			 <div id="{$prefix}_451df1" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_451df1"]}>{$grass["_451df1"]}</p>{$goals["_451df1"]}</div>
			 <div id="{$prefix}_451df2" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_451df2"]}>{$grass["_451df2"]}</p>{$goals["_451df2"]}</div>
			 <div id="{$prefix}_451df3" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_451df3"]}>{$grass["_451df3"]}</p>{$goals["_451df3"]}</div>
			 <div id="{$prefix}_451df4" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_451df4"]}>{$grass["_451df4"]}</p>{$goals["_451df4"]}</div>
			 <div id="{$prefix}_451mid1" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_451mid1"]}>{$grass["_451mid1"]}</p>{$goals["_451mid1"]}</div>
			 <div id="{$prefix}_451mid2" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_451mid2"]}>{$grass["_451mid2"]}</p>{$goals["_451mid2"]}</div>
			 <div id="{$prefix}_451mid3" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_451mid3"]}>{$grass["_451mid3"]}</p>{$goals["_451mid3"]}</div>
			 <div id="{$prefix}_451mid4" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_451mid4"]}>{$grass["_451mid4"]}</p>{$goals["_451mid4"]}</div>
			 <div id="{$prefix}_451mid5" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_451mid5"]}>{$grass["_451mid5"]}</p>{$goals["_451mid5"]}</div>
			 <div id="{$prefix}_451forw1" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_451forw1"]}>{$grass["_451forw1"]}</p>{$goals["_451forw1"]}</div>
eof;
			break;
		}
	
			case 442:{
			$html=<<<eof
			<div id="{$prefix}_442gk" class="dropblpor"><img src='{$shirt}' border='0'/><p{$red["_442gk"]}>{$grass["_442gk"]}</p>{$goals["_442gk"]}</div>
			 <div id="{$prefix}_442df1" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_442df1"]}>{$grass["_442df1"]}</p>{$goals["_442df1"]}</div>
			 <div id="{$prefix}_442df2" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_442df2"]}>{$grass["_442df2"]}</p>{$goals["_442df2"]}</div>
			 <div id="{$prefix}_442df3" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_442df3"]}>{$grass["_442df3"]}</p>{$goals["_442df3"]}</div>
			 <div id="{$prefix}_442df4" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_442df4"]}>{$grass["_442df4"]}</p>{$goals["_442df4"]}</div>
			 <div id="{$prefix}_442mid1" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_442mid1"]}>{$grass["_442mid1"]}</p>{$goals["_442mid1"]}</div>
			 <div id="{$prefix}_442mid2" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_442mid2"]}>{$grass["_442mid2"]}</p>{$goals["_442mid2"]}</div>
			 <div id="{$prefix}_442mid3" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_442mid3"]}>{$grass["_442mid3"]}</p>{$goals["_442mid3"]}</div>
			 <div id="{$prefix}_442mid4" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_442mid4"]}>{$grass["_442mid4"]}</p>{$goals["_442mid4"]}</div>
			 <div id="{$prefix}_442forw1" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_442forw1"]}>{$grass["_442forw1"]}</p>{$goals["_442forw1"]}</div>
			 <div id="{$prefix}_442forw2" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_442forw2"]}>{$grass["_442forw2"]}</p>{$goals["_442forw2"]}</div>
eof;
			break;
		}
	
			case 433:{
			$html=<<<eof
			<div id="{$prefix}_433gk" class="dropblpor"><img src='{$shirt}' border='0'/><p{$red["_433gk"]}>{$grass["_433gk"]}</p>{$goals["_433gk"]}</div>
			 <div id="{$prefix}_433df1" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_433df1"]}>{$grass["_433df1"]}</p>{$goals["_433df1"]}</div>
			 <div id="{$prefix}_433df2" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_433df2"]}>{$grass["_433df2"]}</p>{$goals["_433df2"]}</div>
			 <div id="{$prefix}_433df3" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_433df3"]}>{$grass["_433df3"]}</p>{$goals["_433df3"]}</div>
			 <div id="{$prefix}_433df4" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_433df4"]}>{$grass["_433df4"]}</p>{$goals["_433df4"]}</div>
			 <div id="{$prefix}_433mid1" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_433mid1"]}>{$grass["_433mid1"]}</p>{$goals["_433mid1"]}</div>
			 <div id="{$prefix}_433mid2" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_433mid2"]}>{$grass["_433mid2"]}</p>{$goals["_433mid2"]}</div>
			 <div id="{$prefix}_433mid3" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_433mid3"]}>{$grass["_433mid3"]}</p>{$goals["_433mid3"]}</div>
			 <div id="{$prefix}_433forw1" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_433forw1"]}>{$grass["_433forw1"]}</p>{$goals["_433forw1"]}</div>
			 <div id="{$prefix}_433forw2" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_433forw2"]}>{$grass["_433forw2"]}</p>{$goals["_433forw2"]}</div>
			 <div id="{$prefix}_433forw3" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_433forw3"]}>{$grass["_433forw3"]}</p>{$goals["_433forw3"]}</div>
eof;
			break;
		}
		
		
		case 352:{	
				$html=<<<eof
<div id="{$prefix}_352gk" class="dropblpor"><img src='{$shirt}' border='0'/><p{$red["_352gk"]}>{$grass["_352gk"]}</p>{$goals["_352gk"]}</div>
		 		 <div id="{$prefix}_352df1" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_352df1"]}>{$grass["_352df1"]}</p>{$goals["_352df1"]}</div>
			 	 <div id="{$prefix}_352df2" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_352df2"]}>{$grass["_352df2"]}</p>{$goals["_352df2"]}</div>
				 <div id="{$prefix}_352df3" class="dropbldef"><img src='{$shirt}' border='0'/><p{$red["_352df3"]}>{$grass["_352df3"]}</p>{$goals["_352df3"]}</div>
				 <div id="{$prefix}_352mid1" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_352mid1"]}>{$grass["_352mid1"]}</p>{$goals["_352mid1"]}</div>
				 <div id="{$prefix}_352mid2" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_352mid2"]}>{$grass["_352mid2"]}</p>{$goals["_352mid2"]}</div>
				 <div id="{$prefix}_352mid3" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_352mid3"]}>{$grass["_352mid3"]}</p>{$goals["_352mid3"]}</div>
				 <div id="{$prefix}_352mid4" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_352mid4"]}>{$grass["_352mid4"]}</p>{$goals["_352mid4"]}</div>
				 <div id="{$prefix}_352mid5" class="dropblmed"><img src='{$shirt}' border='0'/><p{$red["_352mid5"]}>{$grass["_352mid5"]}</p>{$goals["_352mid5"]}</div>
				 <div id="{$prefix}_352forw1" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_352forw1"]}>{$grass["_352forw1"]}</p>{$goals["_352forw1"]}</div>
				 <div id="{$prefix}_352forw2" class="dropbldel"><img src='{$shirt}' border='0'/><p{$red["_352forw2"]}>{$grass["_352forw2"]}</p>{$goals["_352forw2"]}</div>
eof;
				break;
		}			
	}
	return $html;
}	

/*
 * Retorna el html de la tabla de jugadores, con sus puntos y goles
 * */
function get_datateam_in_table($tabledata,$total,$equipo,$guest,$manager){
 $htmlgoals="";
 $htmlrojas="";
 $postidlist=($guest==0)?"_local":"_guest";
 $idlistgoals="listgoals".$postidlist;
 $idlistreds="listreds".$postidlist;
 
 $html="<table>";
 $html .="<tr><td colspan='2' class='txtteam'>".$equipo."</td></tr>\n";
 $html .="<tr><td colspan='2' class='txttotal'>".$total."</td></tr>\n";
	foreach($tabledata as $key => $sausage){
 		list($nombre,$demarcacion,$goles,$rojas,$puntos)=explode("#",$sausage);
		switch ($demarcacion){
			case "1": $class="por";
			break;
			case "2": $class="def";
			break;
			case "3": $class="med";
			break;
			case "4": $class="del";
			break;
		}
		for ($i=0;$i<$goles;$i++)
			$htmlgoals .="<li>".$nombre."</li>\n";
		$html .="<tr><td class='txtplayer'>".$nombre."</td><td class='txtptos'>".$puntos."</td></tr>\n";
		
		if ($rojas!=0)
			$htmlrojas .="<li>".$nombre."</li>\n";
	}	
	$html .="</table>";
	$html .="<div id='containgr".$postidlist."'>";
	$html .="<ul class='headlist'>\n<li>Entrenador: <span>".$manager."</span></li>\n</ul>";
	$html .="<ul class='headlist' id='".$idlistgoals."'>\n<li>Goles:</li>\n";
	$html .="<ul>".$htmlgoals."</ul>";
	$html .="</ul>\n";
	$html .="<tr><td colspan='2' class='extradata'>";
	$html .="<ul class='headlist' id='".$idlistreds."'><li>Rojas:</li>\n";
	$html .="<ul>".$htmlrojas."</ul></ul>";
	$html .="</div>";
	
	return $html;
}

/* Retorna en modo cesped el once inicial de una jornada en concreto*/
function get_eleven_in_grass($idequipo,$jornada){
	$oBBDD=BBDD::get_instancia();
		
	// Si la jornada es la cero se toma como jornada la ultima procesada	
	if ($jornada==0){
		$sql="SELECT `numjornada` FROM `".get_pref()."_calendario` WHERE `procesado`=1 ORDER BY `numjornada` DESC LIMIT 1";
		$obj_jornada=$oBBDD->get_resource($sql);
		if (mysqli_num_rows($obj_jornada)!=0){
			$lajornada=mysqli_fetch_object($obj_jornada);
			$jornada=$lajornada->numjornada;
		}
		else
			//por defecto si no hay ninguna jornada procesada, comenzamos en la jornada 1ª
			$jornada=1;
	}
	
	// Si el idequipo es cero se toma como idequipo el del usuario actual
	if ($idequipo==0){
		$idequipo=$_SESSION["team"]["idequipo"];
		$guest=0;
	}	
	else{ //obtengo el id de mi contrincante para la jornada en cuestión
		$guest=1;
		$myid=$_SESSION["team"]["idequipo"];
		$sql="SELECT `idequipo1`, `idequipo2` FROM `".get_pref()."_enfrentamientos` WHERE ";
		$sql .="`numjornada`=".$jornada." AND (`idequipo1`=".$myid." OR `idequipo2`=".$myid.");";
		$obj_data=$oBBDD->get_resource($sql);
		$data=mysqli_fetch_object($obj_data);
		$idequipo=$data->idequipo1;
		if ($idequipo==$myid)
			$idequipo=$data->idequipo2;
	}		
	
	//Pillo los datos del equipo en cuestión	
	$total=0;
	$sql="SELECT `manager1`, `manager2`, `nombre`, `estadio`, `equipacion1` FROM `".get_pref()."_equipos` ";
	$sql .="WHERE `id`=".$idequipo;
	$obj_team=$oBBDD->get_resource($sql);
	$team=mysqli_fetch_object($obj_team);
	
	// Si no tiene defenido un segundo manager le ponemos uno de coña
	if ($team->manager2=="")
		$manager2=get_random_manager();
	else
		$manager2=$team->manager2;	
		
	$shirt=SHIRTS."/".$team->equipacion1;
	$sql="SELECT j.nombre nombre, a.idjugador idjugador, a.idpos idpos, a.capitan capitan,a.puntos puntos, ";
	$sql .="a.jugado jugado, a.roja roja, a.gol gol,a.id idalineacion FROM `".get_pref()."_alineaciones` a ";
	$sql .="INNER JOIN `".get_pref()."_jugadores` j ON (j.id=a.idjugador) ";
	$sql .="WHERE (a.jornada=".$jornada." AND a.idequipo=".$idequipo.")";
	$obj_alin=$oBBDD->get_resource($sql);
	$hayonce=mysqli_num_rows($obj_alin);
	while ($alin=mysqli_fetch_object($obj_alin)){
		if ( ($alin->puntos==0) &&  ($alin->jugado==0) )
			$txtpto="-"; //no ha jugado el partido
		else
			$txtpto=$alin->puntos;	
		$grass[$alin->idpos]=$alin->nombre;
		$editnumbers[$alin->idpos]=$alin->idalineacion."#".$alin->nombre."#".$alin->gol."#".$alin->roja."#".$alin->idjugador;
		$goalhtml="";
		if ( $alin->gol > 0 ){
			$goalhtml="<div class='goles'>";
			for ($i=0;$i<$alin->gol;$i++)
				$goalhtml .="<img src='images/gol.png'>";
			$goalhtml .="</div>";	
			
		}
		
		$goals[$alin->idpos]=$goalhtml;
		$redclass=" class='placebo'";
		if ( $alin->roja!=0 )
			$redclass=" class='roja'";
		$red[$alin->idpos]=$redclass;
		
		//puntos totales del partido
		$total +=$alin->puntos;
		$idtactic=substr($alin->idpos, 1,3);	
	}
	$html=<<<eof
	<div class="rotulo">{$team->nombre}</div>
	<div class="managersmatches">
		<div class="minimanager1">{$team->manager1}</div>
		<div class="minimanager2">{$manager2}</div>
	</div>
eof;

	if ($guest==0){
		$html .="<div id='localscore' class='score' title='".$total." puntitos eh ?'>".$total."</div>";
		$prefix="";
	}	
	else{
		$html .="<div id='guestscore' class='score'>".$total."</div>";
		
		$sql="SELECT `numjornada`, `fecha`, `vuelta`, `jornadant` FROM `".get_pref()."_calendario`";
		$sql .=" WHERE `numjornada`=".$jornada.";";
		$obj_data=$oBBDD->get_resource($sql);
		$data=mysqli_fetch_object($obj_data);
		$html .=<<<eof
		<div class="div_info" id="datamatches">
			<fieldset>
				 <legend>informaci&oacute;n</legend>
				<ul>
					<li><span>fecha: </span>{$data->fecha}</li>
					<li><span>jornada liga: </span>{$data->numjornada}</li>
					<li><span>jornada antiliga: </span>{$data->jornadant}</li>
					<li><span>vuelta: </span>{$data->vuelta}&ordf;</li>
				<ul>
			</fieldset>
		</div>
eof;
		$prefix="_";
	}	
		
	if ($hayonce<11)
		$html .="<div class='nodata'>Todavía sin once</div>";
	else{
		// Si es mi equipo, establezco el vector de sesion con mi equipo
		if ($guest==0){
			unset($_SESSION["editnumbers"]);
			//variable de sesion que contiene el idposicion y el idalineacion de mi equipo para editarlo y poner goles o rojas
			$_SESSION["editnumbers"]=$editnumbers;
		}
		
		$html .=get_tactic_in_grass($idtactic,$prefix,$shirt,$grass,$goals,$red);
	}	
	
	$html .="<div class='stadiummatches'>".ucwords(strtolower($team->estadio))."</div>";
	
	return $html;
}


function get_datamatch ($jornada,$idequipo,$guest,$shirt,$equipo,$manager){
	$oBBDD=BBDD::get_instancia();
	
	$grass=array();
	$tabledata=array();
	$editnumbers=array();
	$goals=array();
	$red=array();
	$total=0;
	$prefix="g"; //prefijo de css para equipo visitante
	$sql="SELECT j.nombre nombre, j.demarcacion demarcacion, a.idjugador idjugador, a.idpos idpos, a.capitan capitan,a.puntos puntos, ";
	$sql .="a.jugado jugado, a.roja roja, a.gol gol,a.id idalineacion FROM `".get_pref()."_alineaciones` a ";
	$sql .="INNER JOIN `".get_pref()."_jugadores` j ON (j.id=a.idjugador) ";
	$sql .="WHERE (a.jornada=".$jornada." AND a.idequipo=".$idequipo.") ORDER by demarcacion ASC";

	$obj_alin=$oBBDD->get_resource($sql);
	$hayonce=mysqli_num_rows($obj_alin);
	while ($alin=mysqli_fetch_object($obj_alin)){
		if ( ($alin->puntos==0) &&  ($alin->jugado==0) )
			$txtpto="-"; //no ha jugado el partido
		else
			$txtpto=$alin->puntos;
			
		$grass[$alin->idpos]=$alin->nombre;
		$editnumbers[$alin->idpos]=$alin->idalineacion."#".$alin->nombre."#".$alin->gol."#".$alin->roja."#".$alin->idjugador;
		$goalhtml="";
		if ( $alin->gol > 0 ){
			$goalhtml="<div class='goles'>";
			for ($i=0;$i<$alin->gol;$i++)
				$goalhtml .="<img src='images/gol.png'>";
			$goalhtml .="</div>";	
			
		}
		
		$goals[$alin->idpos]=$goalhtml;
		$redclass=" class='placebo'";
		if ( $alin->roja!=0 )
			$redclass=" class='roja'";
		$red[$alin->idpos]=$redclass;
		
		//puntos totales del equipo
		$total +=$alin->puntos;
		$idtactic=substr($alin->idpos, 1,3);
		
		$tabledata[$alin->idjugador]=$alin->nombre."#".$alin->demarcacion."#".$alin->gol."#".$alin->roja."#".$txtpto;
	}
	
	if ($guest==0){
			unset($_SESSION["editnumbers"]);
			//variable de sesion que contiene el idposicion y el idalineacion de mi equipo para editarlo y poner goles o rojas
			$_SESSION["editnumbers"]=$editnumbers;
			$prefix="";
	}
	$htmlgrass=get_tactic_in_grass($idtactic,$prefix,$shirt,$grass,$goals,$red);
	$tablehtml=get_datateam_in_table($tabledata,$total,$equipo,$guest,$manager);
	$vresult=array('htmlgrass'=>$htmlgrass, 'puntos'=>$total,'tablehtml'=>$tablehtml);
	
	return $vresult;
}


function get_list_goals_and_reds($jornada){
	$oBBDD=BBDD::get_instancia();
	
	$tabledata=array();
	$goals=array();
	$red=array();
	$htmlgoals="";
	$htmlrojas="";
	$html="";
	
	// Si la jornada es la cero se toma como jornada la ultima procesada	
	if ($jornada==0){
		$sql="SELECT `numjornada` FROM `".get_pref()."_calendario` WHERE `procesado`=1 ORDER BY `numjornada` DESC LIMIT 1";
		$obj_jornada=$oBBDD->get_resource($sql);
		if (mysqli_num_rows($obj_jornada)!=0){
			$lajornada=mysqli_fetch_object($obj_jornada);
			$jornada=$lajornada->numjornada;
		}
		else
			//por defecto si no hay ninguna jornada procesada, comenzamos en la jornada 1ª
			$jornada=1;
	}
	
	$myid=$_SESSION["team"]["idequipo"];
	
	$sql="SELECT j.nombre nombre, a.roja roja, a.gol gol FROM `".get_pref()."_alineaciones` a ";
	$sql .="INNER JOIN `".get_pref()."_jugadores` j ON (j.id=a.idjugador) ";
	$sql .="WHERE (a.jornada=".$jornada." AND a.idequipo=".$myid.") ORDER by demarcacion ASC";

	$obj_alin=$oBBDD->get_resource($sql);
	$hayonce=mysqli_num_rows($obj_alin);
	while ($alin=mysqli_fetch_object($obj_alin)){
		for ($i=0;$i<$alin->gol;$i++)
			$htmlgoals .="<li>".$alin->nombre."</li>\n";
		if ($alin->roja!=0)
			$htmlrojas .="<li>".$alin->nombre."</li>\n";
	}
	
	$html .="<ul class='headlist'>\n<li>Entrenador: <span>".$_SESSION["team"]["manager1"]."</span></li>\n</ul>";
	$html .="<ul class='headlist' id='listgoals_local'>\n<li>Goles:</li>";
	$html .="<ul>".$htmlgoals."</ul>";
	$html .="</ul></td></tr>\n";
	$html .="<tr><td colspan='2' class='extradata'>";
	$html .="<ul class='headlist' id='listreds_local'><li>Rojas:</li>";
	$html .="<ul>".$htmlrojas."</ul></ul>";
		
	return $html;
}

function get_report_match($jornada){
	$oBBDD=BBDD::get_instancia();
	$xml=<<<eof
		<?xml version="1.0" encoding="utf-8"?>
		<data>
		<myteam>
			<nombre>{$_SESSION["team"]["nombre"]}</nombre>
			<manager1>{$_SESSION["team"]["manager1"]}</manager1>
			<manager2>{$_SESSION["team"]["manager2"]}</manager2>
			<estadio>{$_SESSION["team"]["estadio"]}</estadio>
		</myteam>
		
eof;
			
	// Si la jornada es la cero se toma como jornada la ultima procesada	
	if ($jornada==0){
		$sql="SELECT `numjornada` FROM `".get_pref()."_calendario` WHERE `procesado`=1 ORDER BY `numjornada` DESC LIMIT 1";
		$obj_jornada=$oBBDD->get_resource($sql);
		if (mysqli_num_rows($obj_jornada)!=0){
			$lajornada=mysqli_fetch_object($obj_jornada);
			$jornada=$lajornada->numjornada;
		}
		else
			//por defecto si no hay ninguna jornada procesada, comenzamos en la jornada 1ª
			$jornada=1;
	}
	
	//obtengo el id de mi contrincante para la jornada en cuestión
	$myid=$_SESSION["team"]["idequipo"];
	$sql="SELECT `idequipo1`, `idequipo2` FROM `".get_pref()."_enfrentamientos` WHERE ";
	$sql .="`numjornada`=".$jornada." AND (`idequipo1`=".$myid." OR `idequipo2`=".$myid.");";
	$obj_data=$oBBDD->get_resource($sql);
	$data=mysqli_fetch_object($obj_data);
	$idequipo=$data->idequipo1;
	if ($idequipo==$myid)
		$idequipo=$data->idequipo2;
	
	//Pillo los datos del equipo rival	
	$total=0;
	$sql="SELECT `manager1`, `manager2`, `nombre`, `estadio`, `equipacion1` FROM `".get_pref()."_equipos` ";
	$sql .="WHERE `id`=".$idequipo;
	$obj_team=$oBBDD->get_resource($sql);
	$team=mysqli_fetch_object($obj_team);
	
	// Si no tiene defenido un segundo manager le ponemos uno de coña
	if ($team->manager2=="")
		$manager2=get_random_manager();
	else
		$manager2=$team->manager2;
	
	$shirt=SHIRTS."/".$team->equipacion1;
	
	$xml .=<<<eof
<myenemy>
			<nombre>{$team->nombre}</nombre>
			<manager1>{$team->manager1}</manager1>
			<manager2>{$team->manager2}</manager2>
			<estadio>{$team->estadio}</estadio>
		</myenemy>
		
eof;

	$sql="SELECT `numjornada`, `fecha`, `vuelta`, `jornadant` FROM `".get_pref()."_calendario`";
	$sql .=" WHERE `numjornada`=".$jornada.";";
	$obj_data=$oBBDD->get_resource($sql);
	$data=mysqli_fetch_object($obj_data);
	
	$xml .=<<<eof
<match>
			<jornada>{$data->numjornada}</jornada>
			<fecha>{$data->fecha}</fecha>
			<vuelta>{$data->vuelta}</vuelta>
			<jornada_antiliga>{$data->jornadant}</jornada_antiliga>
		</match>
		
eof;
		
	
	// Puntos y alineaciones en el cesped del equipo visitante
	$vdatamatch=get_datamatch ($jornada,$idequipo,$guest=1,$shirt,$team->nombre,$team->manager1);
	$enemy_htmlgrass=$vdatamatch['htmlgrass'];
	$enemy_tablehtml=$vdatamatch['tablehtml'];
	$enemy_ptos=$vdatamatch['puntos'];
	
	// Puntos y alineaciones en el cesped del equipo local
	$shirt=SHIRTS."/".$_SESSION["team"]["equipacion"];
	$idequipo=$_SESSION["team"]["idequipo"];
	$vdatamatch=get_datamatch ($jornada,$idequipo,$guest=0,$shirt,$_SESSION["team"]["nombre"],$_SESSION["team"]["manager1"]);
	$local_htmlgrass=$vdatamatch['htmlgrass'];
	$local_tablehtml=$vdatamatch['tablehtml'];
	$local_ptos=$vdatamatch['puntos'];
	
	$htmlgrass=$local_htmlgrass.$enemy_htmlgrass;
	
	$xml .=<<<eof
<htmlgrass>
			{$htmlgrass}
		</htmlgrass>
<local_tablehtml>
		{$local_tablehtml}
		</local_tablehtml>
<enemy_tablehtml>
		{$enemy_tablehtml}
		</enemy_tablehtml>
		<result>
			<local>{$local_ptos}</local>
			<enemy>{$enemy_ptos}</enemy>
		</result>
		</data>
		</xml>
eof;

	return $xml;
}

function get_select_matches(){
	$oBBDD=BBDD::get_instancia();
	$current=1;
	$html="<select id='listformmatches'>";
	$html .="<option value=0 selected>última jornada procesada</option>\n";
	$html .="<optgroup label='1ª Vuelta'>\n";
	$sql="SELECT DISTINCT a.jornada numjornada, c.fecha fecha,c.vuelta vuelta FROM `".get_pref()."_alineaciones` a INNER JOIN `".get_pref()."_calendario` c";
	$sql .=" ON (a.jornada=c.numjornada) WHERE idequipo=".$_SESSION["team"]["idequipo"]." order by jornada ASC;";
	$obj_calendar=$oBBDD->get_resource($sql);
	while ($calendar=mysqli_fetch_object($obj_calendar)){
		if ($current!=$calendar->vuelta){
			$html .="</optgroup>\n<optgroup label='".$calendar->vuelta."&ordf;. Vuelta'>\n";
			$current=$calendar->vuelta;
		}
			$html .="<option value=".$calendar->numjornada.">".$calendar->numjornada."&ordf; jornada - ".$calendar->fecha."</option>\n";
	}
	$html .="</optgroup>\n</select>";
	
	return $html;
}


function set_form_numbers($idpos){
	$html="Arrg, ha ocurrido un error, inténtalo de nuevo o habla con Mario";
	$disabled=($_SESSION["year"]!=0)?"disabled":"";
 
	if ( isset ($_SESSION["editnumbers"]) ){
		list($idalineacion,$nombre,$gol,$roja,$idjugador)=explode("#",$_SESSION["editnumbers"][$idpos]);
		if ($roja==0){
			$htmlred=<<<eof
			<select id='reds' {$disabled}>
				<option value="0" selected>no</option><br/>
				<option value="1">si</option>
			</select>
eof;
}
	else{
			$htmlred=<<<eof
			<select id='reds' {$disabled}>
				<option value="1" selected>si</option><br/>
				<option value="0">no</option>
			</select>
eof;
}			
		$html=<<<eof
		<div class="div_info" id="div_edit_numbers">
			<div id="editjugadornumbers" class="rotulo">{$nombre}</div>
			<form id="form_edit_numbers" action="#">
			<table id="table_form_numbers">
				<input type="hidden" id="idalineacion" value="{$idalineacion}" />
				<input type="hidden" id="idjug" value="{$idjugador}" />
				<tr><td><label for="goals">goles</label></td><td><label for="reds">tarjeta roja</label></td></tr>
				<tr>
					<td><input type="text" name="goals" id="goals" value="{$gol}"  {$disabled}/></td>
					<td>
						{$htmlred}				
					</td>
				</tr>
			</table>
			<table id="table_button_form_numbers">
				<tr>
					<td><input type="button" id="btn_cancel_form_numbers" value="cancelar"/></td>
					<td><input type="button" id="btn_set_form_numbers" value="aceptar" {$disabled}/></td>
				<tr/>
			</table>	
			</form>
		</div>
eof;
	}
	return $html;
}

function set_numbers($goals,$reds,$idalineacion,$idpos,$idjugador){
	$oBBDD=BBDD::get_instancia();
	// Primero obtenemos los goles y rojas de este partido
	$sql="SELECT `gol`,`roja` FROM `".get_pref()."_alineaciones` WHERE `id`=".$idalineacion;
	$obj_alin=$oBBDD->get_resource($sql);
	$alin=mysqli_fetch_object($obj_alin);
	
	$oldred=$alin->roja;
	$oldgoals=$alin->gol;
	$diffreds=$reds-$oldred;
	$diffgoals=$goals-$oldgoals;
	
	$sql="UPDATE `".get_pref()."_alineaciones` SET `roja`=".$reds.", `gol`=".$goals." WHERE `id`=".$idalineacion;
	$oBBDD->set_resultados($sql);
	//Los goles totales ahora se pillan del fichero de la LFM, sólo actualizamos las rojas
	$sql="UPDATE `".get_pref()."_jugadores` SET `rojas`=`rojas`+".$diffreds." WHERE `id`=".$idjugador;
	$oBBDD->set_resultados($sql);
	if ( isset ($_SESSION["editnumbers"]) ){
		list($idalineacion,$nombre,$gol,$roja,$idjugador)=split("#",$_SESSION["editnumbers"][$idpos]);
		$_SESSION["editnumbers"][$idpos]=$idalineacion."#".$nombre."#".$goals."#".$reds."#".$idjugador;
	}
	else
		die ("Arrrg, ha ocurrido un error, comienzas de nuevo ??");	
	return 0;
}

?>


