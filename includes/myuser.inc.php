<?php
/*
 *      myteam.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");



function get_form_user(){
	$oBBDD=BBDD::get_instancia();
	$idusuario=$_SESSION["userinfo"]["idusuario"];
	$sql="SELECT * FROM `users` WHERE `id`=".$idusuario;
	$obj_user=$oBBDD->get_resource($sql);
	$user=mysqli_fetch_object($obj_user);
	$html=<<<eof
	<form name="form_data_user" id="form_data_user" method="POST" action="#">
	<div id="div_form_data_user">
	<table>
		<tr><td>usuario:</td><td><input type="text" id="username" value="{$user->usuario}" disabled="disabled"></td></tr>
		<tr><td>password:</td><td><input type="password" id="passwd" value="{$user->password}"></td></tr>
		<tr><td>confirma la password:</td><td><input type="password" id="rpasswd" value="{$user->password}"></td></tr>
		<tr><td>m&oacute;vil:</td><td><input type="text" id="movil" value="{$user->movil}"></td></tr>
		<tr><td>twitter:</td><td><input type="text" id="twitter" value="{$user->twitter}"></td></tr>
	</table>
	</div>
	<div id="btn_form_data_user">
		<table>
			<tr>
				<td><input class="button" type="button" id="btn_cancel_form_data_user" value="cancelar"></td>
				<td><input class="button" type="button" id="btn_send_form_data_user" value="guardar y volver"></td>
			</tr>
		</table>
	</div>
	</form>
eof;
	
	return $html;

}


function send_data_user($passwd,$movil,$twitter){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `password` FROM `users` where `id`=".$_SESSION["userinfo"]["idusuario"];
	$obj_user=$oBBDD->get_resource($sql);
	$user=mysqli_fetch_object($obj_user);
	if ($user->password!=$passwd)
		$passwd=sha1($passwd);
	$sql="UPDATE `users` SET `password`='".$passwd."', `movil`=".$movil.", `twitter`='".$twitter."' WHERE `id`=".$_SESSION["userinfo"]["idusuario"];
	$oBBDD->set_resultados($sql);
	return 0;
}
?>


