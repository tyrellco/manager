<?php
/*
 *      admin.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


session_save_path(dirname(__FILE__)."/tmp");
session_start();
if ( !isset($_SESSION["userinfo"]) )	header ("Location: login.php");
require_once(dirname(__FILE__)."/conf/config.php");
require_once (dirname(__FILE__)."/includes/admin.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>antiliga-manager</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link href="css/site.css" rel="stylesheet" type="text/css">
<link href="css/manager.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

<script language="Javascript" src="libjs/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" src="js/admin.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">

	$(document).ready(function(){
		$("#loading").hide();
		$("#message").hide();
		$("#popup").hide();
		$("#op1.opadmin").bind("click",function (){
			do_get_form_datafile();
		});
		$("#op2.opadmin").bind("click",function (){
			do_get_form_faults();
		});
		$("#op3.opadmin").bind("click",function (){
			//do_get_form_allplayers();
			alert("en obras");
		});

		$("#btn_come_back").bind("click",function (){
			window.location.href="index.php";
		});

		
});

</script>
</head>

<body>
<div id="main">
	<div id='message'></div>
	<div id="loading">procesando...</div>
	<div id="userinfo"></div>
	<div id="leftcontainer" class="menuadmin"><?php echo get_menu_admin(); ?></div>
	<div id="rightcontainer" class="rightcontaineradmin">	<?php echo process_datafile();?>
	</div>
	<div id="div_come_back" class="come_back_admin">
		<input type="button" id="btn_come_back" class="button" value="volver"/>
	</div>
</body>
</html>

