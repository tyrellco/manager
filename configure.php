<?php
/*
 *      configure.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


session_save_path(dirname(__FILE__)."/tmp");
session_start();
if ( !isset($_SESSION["userinfo"]) )	header ("Location: login.php");
require_once(dirname(__FILE__)."/conf/config.php");
require_once (dirname(__FILE__)."/includes/configure.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>antiliga-manager</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link href="css/site.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<script language="Javascript" src="libjs/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" src="js/configure.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">

	$(document).ready(function(){
		$("#loading").hide();
		$("#message").hide();
		$("#btn_comeback").bind("click",function (){ window.location.href="index.php";});
		$("#form_team input").keydown (check_for_enter_configure);
		//$("#form_team input").bind("focus",function (){$(this).css("background-color","#DCDCF3"); });
		$("#form_team input.required").bind("blur",
			function (){
								//Eliminamos los espacios en blanco al princio y final de la cadena
									var value=$(this).val().replace(/^\s*|\s*$/g,"");
									var id=$(this).attr("id");
									$("label[for='"+id+"']").removeAttr("class");	
									if (value.length > 0){
										if ($(this).attr("id")!="equipacion1")
											set_data_configure($(this).attr("id"),value);
										$("label[for='"+id+"']").addClass("ok");
									}	
									else
										$("label[for='"+id+"']").addClass("ko");
			});
			
			$("#form_team input.optional").bind("blur",
			function (){
									//Eliminamos los espacios en blanco al princio y final de la cadena
									var value=$(this).val().replace(/^\s*|\s*$/g,"");
									var id=$(this).attr("id");
									$("label[for='"+id+"']").removeAttr("class");	
									if (value.length > 0)
											set_data_configure($(this).attr("id"),value);
									else
											set_data_configure($(this).attr("id"),"");		
	
			});
			
			$("#form_team input.number").bind("blur",
			function (){
									//Eliminamos los espacios en blanco al princio y final de la cadena
									var value=$(this).val().replace(/^\s*|\s*$/g,"");
									var id=$(this).attr("id");
									$("label[for='"+id+"']").removeAttr("class");	
									if ( isNaN(value) || (value.length<=0) )
											$("label[for='"+id+"']").addClass("ko");
									else{
										set_data_configure($(this).attr("id"),value);
										$("label[for='"+id+"']").addClass("ok");
									}	
			});
			
			$("#form_team input#equipacion1.required").bind("click",
			function (){
					$(this).blur();
					do_get_shirts_list();
			});
			
		 check_form_team();
		 do_get_summary_data();
			
});

</script>
</head>

<body>
<div id="main">
	<div id='message'></div>
	<div id="loading">procesando...</div>
	<div id="leftcontainer"><?php echo get_form_team();?></div>
	<div id="rightcontainer" class="rightconfigure"><?php echo get_summary_configure_team(0);?></div>
</body>
</html>

