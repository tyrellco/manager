<?php
/*
 *      myteam.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
session_save_path(dirname(__FILE__)."/tmp");
session_start();
if ( !isset($_SESSION["userinfo"]) )	header ("Location: login.php");
require_once(dirname(__FILE__)."/conf/config.php");
require_once (dirname(__FILE__)."/includes/desktop.inc.php");
get_datateam($_SESSION["userinfo"]["idequipo"]);
$shirt=SHIRTS."/".$_SESSION["team"]["equipacion"];
$status=get_team_status();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>antiliga-manager</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="alternate" type="text/xml" title="RSS 2.0" href="rss.php" />
<link href="css/site.css" rel="stylesheet" type="text/css">
<link href="css/desktop.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<script language="Javascript" src="libjs/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery-ui-personalized-1.5.2.packed.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery.kwicks-1.5.1.pack.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" src="js/desktop.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">
		$(document).ready(function(){
		load_menu_links(<?php echo $_SESSION["userinfo"]["rol"]?>,<?php echo $status?>);	
		$("#loading").hide();
		$("#message").hide();
		$("#popup").hide();
		$("#listlastyears").change(function () {
					do_set_pref($(this).val(),$('#listlastyears :selected').text());
  	});
  	
		$("#userinfo ul li#username").bind("click",function (){
			window.location.href="myuser.php";
		});
		$("#userinfo ul li#logout").bind("click",function (){
			do_logout();
		});	
		$("#userinfo ul li#help").bind("click",function (){
			alert("Ayudarte podría....");
			});	
		$("#summary_team fieldset legend").css("background-image","url('<?php echo $shirt;?>')");
		init_button_results();
		$('#clasificacion > ul').tabs({ fx: { height: 'toggle', opacity: 'toggle' } });
		$(".tabledata table tr td.nomclas").bind("click",function (){
					do_get_enemydata($(this).attr("id"));
			});
		$().ready(function() {
				$('.menu').kwicks({
					max : 205,
					spacing : 5
				});
			});	
});

</script>
</head>

<body>
<div id="main">
	<div id='message'></div>
	<div id="loading">procesando...</div>
	<div id="userinfo"><?php echo get_info_user();?></div>
	<div id="border_menu"></div>
	<div id="main_menu">
		<ul class="menu horizontal">
				<li id="menu_1"></li>
				<li id="menu_2"></li>
				<li id="menu_3"></li>
				<li id="menu_4"></li>
				<li id="menu_5"></li>
			</ul>
	</div>
	<div id="logoapp" class="desktop"></div>
	<div id="summary_team"><?php echo get_summary_team();?></div>
	<div id="lastest_results"><?php echo get_lastest_results(0)?></div>
	<div id="allclasifications">
	<fieldset id="clasificacion">
		<legend class="rotulo">clasificaci&oacute;n</legend>
		<ul class="tabnav">
			<li><a href="#general">general</a></li>
			<li><a href="#vuelta">vuelta</a></li>
			<li><a href="#regularidad">regularidad</a></li>
		</ul>
		<div id="general" class="tabledata"><?php echo get_clasification(1);?></div>
		<div id="vuelta" class="tabledata"><?php echo get_clasification(2);?></div>
		<div id="regularidad" class="tabledata"><?php echo get_clasification(3);?></div>
	</fieldset>
	</div>
	<div id="lastupdate" class="leyenda"><?php echo get_last_time_update();?></div>
	<div id="lastyears"><?php echo get_list_lastyears();?></div>
	<div id="popup" class="enemydata"></div>
</body>
</html>

