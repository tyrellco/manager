<?php
/*
 *      login.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
session_save_path(dirname(__FILE__)."/tmp");
session_start();
require_once(dirname(__FILE__)."/conf/config.php");
require_once (dirname(__FILE__)."/includes/global.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>antiliga-manager</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="alternate" type="text/xml" title="RSS 2.0" href="rss.php" />
<link href="css/login.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<script language="Javascript" src="libjs/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="js/login.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">
	$(document).ready(function(){
	$("#loginbtn a").bind("click",function (){
			do_login($("#username").val(),$("#userpasswd").val());
	})	;
	$("#login input").keydown (check_for_enter_login);
});
</script>
</head>

<body>
<div id="main">
<div id="logoapp" class="login"></div>
<div id="login">
	<div id="logologin"></div>
		<div id="loginuser">
			<label for="username">usuario:</label>
			<input type="text" id="username" name="username" value=""/>
		</div>
		<div id="loginpasswd">
			<label for="password">contrase&ntilde;a:</label>
			<input type="password" id="userpasswd" name="userpasswd" value=""/>
		</div>
		<div id="loginbtn"><a href="#"><img src="images/btnlogon.png" /></a></div>
		<div id="about"><?php echo about();?></div>
</div>
<div id="msglogin"></div>	
<div id="techs"></div>	
</body>
</html>
