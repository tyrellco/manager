<?php

/*  m.desktop.php
 *
 *  Copyright (C) 2014  Mario Rubiales Gómez <mariorubiales@gmail.com>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
session_save_path(dirname(__FILE__)."/tmp");
session_start();
if ( !isset($_SESSION["userinfo"]) )	header ("Location: login.php");
require_once(dirname(__FILE__)."/conf/config.php");
require_once (dirname(__FILE__)."/includes/manager.inc.php");
unset ($_SESSION["alineacion"]);
unset ($_SESSION["capitan"]);
?>
 
<!DOCTYPE html>
<html>
<head>
	<title>antiliga-manager</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="css/mobile/antiliga-mobile.min.css"/>
	<link rel="stylesheet" href="css/mobile/jquery.mobile.icons.min.css"/>
	<link rel="stylesheet" href="css/mobile/jquery.mobile.structure-1.4.3.min.css"/>
	<link rel="stylesheet" href="css/m.site.css"/>
	<script src="libjs/mobile/jquery-1.11.1.min.js"></script>
	<script src="libjs/mobile/jquery.mobile-1.4.3.min.js"></script>
	<script src="js/global.js"></script>
	<script src="js/manager.js"></script>
	<script>
		$(document).on('pagecreate',function () {
			$("#main_container form#form_team_mobi a#btn_send_team_mobi").bind("click",function (){
				do_prepare_eleven_mobi();
			});
			
			$("#main_container form#form_team_mobi a#btn_reset_form_mobi").bind("click",function (){
				window.location.href="desktop.php";
			});
		});	

</script>	
</head>
<body>
<!-- MAIN -->
<div data-role="page" id="main">
	<div role="content" class="ui-content">
		<div id="main_container">
		<?php echo get_team_mobi($_SESSION["userinfo"]["idequipo"]); ?>
		</div>
	</div>
</div>
<!-- /MAIN -->
</body>
</html>
