<?php
/*This file is part of manager.
*
* manager is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.

* manager is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Foobar; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
* Author: Mario Rubiales Gomez <mariorubiales@gmail.com>
* Date: 22-may-2007 0:32:13
* Description: Clase que realiza cualquier tipo de operación sobre la BBDD, las operaciones y de consulta
* y/o actualización llevan control de transacciones siempre y cuando la tabla implicada sea del tipo InnoDB.
* Clase desarrollada siguiendo el patrón singleton.
* 
* Update: 27/08/2019: Adapted to PHP 7.X
*/

require_once (dirname(__FILE__)."/../conf/config.php");

class BBDD {

	private static $instancia;
	private $server=SERVER;
	private $bbddname=BBDD;
	private $bbdduser=USER;
	private $bbddpasswd=PASSWD;
	private $link;
	private $resultado;
	private $error;
	private $txterror;

	private function __construct(){
		$this->link=@mysqli_connect($this->server,$this->bbdduser,$this->bbddpasswd);
		if (!$this->link){
			die("<br/>Error de conexión con la bbdd:".mysqli_error($this->link));
		}
		if (!@mysqli_select_db($this->link, $this->bbddname)){
			die("<br/>Error al seleccionar la bbdd:".mysqli_error($this->link));
		}
		return $this->link;
	}

	public static function get_instancia(){
		if (self::$instancia===null)
			self::$instancia=new BBDD();
		return self::$instancia;
	}

	public function get_resource($sql){
		$this->resultado=@mysqli_query($this->link,$sql);
		if (!$this->resultado){
			echo "<br/>Error al ejecutar la consulta en la BBDD:". mysqli_error($this-link)."<br/>$sql<br/><br/>";
			die ("Hasta otra ...");
		}

		return $this->resultado;
	}

	public function set_resultados($sql){
		$this->resultado=@mysqli_query($this->link,$sql);
		if (!$this->resultado){
			echo "<br/>Error al ejecutar actualización y/o inserción en la BBDD:". mysqli_error($this-link)."<br/>$sql<br/><br/>";
			die ("Hasta otra ...");
		}
	}

	public function __clone(){
		trigger_error("ERROR: Prohibido clonar una clase singleton",E_USER_ERROR);
	}
}

?>
