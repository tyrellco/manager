<?php
/*
 *      MoseLog.class.php
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
 

 class MoseLog{
 	private $logpath;
	private $separator;
	private $multilog;
	
	public function __construct($logpath="",$separator="",$multilog=NULL,$prefix=""){
		$this->logpath=($logpath=="")?LOGPATH:$logpath;
		$this->separator=($separator=="")?LOGSEPARATOR:$separator;
		$this->multilog=(is_null($multilog))?MULTILOG:$multilog;
		$this->prefix=($prefix=="")?PREFIX:$prefix;
	}
	
	//Al ser privado este método sólo podrá ser invocado desde la propia clase.
	private function setlog ($type,$body){
		$logname=($this->multilog)?$this->prefix."_".strtolower($type).".log":$this->prefix.".log";
		$filelog=realpath(dirname(__FILE__)."/../".$this->logpath."/".$logname);
		$now=date("Y-m-d H:i:s");
		$sausage=$now.$this->separator.$type.$this->separator.$body."\n";
		$fh = fopen($filelog, 'a+');
		fwrite($fh, $sausage);
		fclose($fh);	
	}
	
	public function info ($body){
		self::setlog("INFO",$body);
	}
	
	public function notice ($body){
		self::setlog("NOTICE",$body);
	}
	
	public function warn ($body){
		self::setlog("WARNING",$body);
	}
	
	public function error ($body){
		self::setlog("ERROR",$body);
	}
	
 }
 
?>